/*
 ============================================================================
 Name        : main.c
 Author      : ie699416
 Version     : 1.1
 Description : Main program file.
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int main(void) {

	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);

	FILE *f = fopen("Archivo.txt", "w");
	if (f != NULL) {
		fprintf(f, "Adio archivo. \n");
		for (int i = 0; i < 20; i++)
			fprintf(f, "%d", i);

	}
	fclose(f);

	f = fopen("Archivo.txt", "r");

	char c;
	c = fgetc(f);
	while (c != EOF) {
		printf("%c", c);
		c = fgetc(f);
	}

	fclose(f);

	return 0;
}
