#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "dictionary.h"

int main(void) {
	/*Eclipse buffers flush commands*/
	setbuf(stdout, NULL);

	Dictionary d = dict_create();

	dict_set(d, "adi�s!", "Despedida en espa�ol");
	dict_set(d, "A�o", "365 d�as");
	dict_set(d, "Acabar", "Terminar");
	dict_set(d, "Az�cAr", "Disac�rido");
	dict_set(d, "azucar", "�A bailar!");
	dict_set(d, "adi�s!", "Despedida en espa�ol");

	dict_get(d, "Az�car");    // Imprime: �A bailar!
	dict_get(d, "Az�car!");   // Imprime: Word not found

	dict_get(d, "adi�s!");
	dict_destroy(d);

	return 0;
}
