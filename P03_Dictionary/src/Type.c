
#include <string.h>
#include "Type.h"

Type Int(int n){
	int * new=(int *)malloc(sizeof(int));
	*new=n;
	return (Type)new;
}

Type Float(float f){
	float * new=(float *)malloc(sizeof(float));
	*new=f;
	return (Type)new;
}

Type Char(char c){
	char * new=(char *)malloc(sizeof(char));
	*new=c;
	return (Type)new;
}

Type String(char *s){
	char * new=strdup(s);
	return (Type)new;
}

int  Type2Int(Type n){
	return *(int*)n;
}

float  Type2Float(Type n){
	return *(float*)n;
}

char Type2Char(Type n){
	return *(char*)n;
}

char * Type2String(Type n){
	return (char*) n;
}
