#include "dictionary.h"

/******************************************************************************/
/*Sub functions*/

char accentCheck(char letter) {
	if (letter == '�')
		letter = 'a';
	else if (letter == '�')
		letter = 'e';
	else if (letter == '�')
		letter = 'i';
	else if (letter == '�')
		letter = 'o';
	else if (letter == '�')
		letter = 'u';
	else if (letter == '�')
		letter = 'A';
	else if (letter == '�')
		letter = 'E';
	else if (letter == '�')
		letter = 'I';
	else if (letter == '�')
		letter = 'O';
	else if (letter == '�')
		letter = 'U';

	return letter;

}
unsigned int letter2int(char letter) {
	letter = accentCheck(letter);

	if (letter >= 'A' && letter <= 'Z')
		return letter - 'A'; //Between 0 - 25
	else if (letter >= 'a' && letter <= 'z')
		return letter - 'a'; //Between 0 - 25
	else if (letter == '�' || letter == '�')
		return ENIE;
	return UNDERSCORE;
}

char* keygen(char *word) {
	int n = strlen(word);
	char *key = (char*) calloc(n, sizeof(char));
	char letter;
	for (int i = 0; i < n; i++) {
		letter = accentCheck(word[i]);
		if ((letter >= 'A' && letter <= 'Z') || letter == '�') {
			key[i] = word[i];
		} else if ((letter >= 'a' && letter <= 'z') || letter == '�') {
			key[i] = toupper(letter);
		} else {
			key[i] = '_';
		}
	}

	return key;
}

Boolean keyCmp(char *k1, char *k2) {
	if (strlen(k1) == strlen(k2)) {
		for (int i = 0; i < strlen(k1); i++) {
			if (k1[i] == k2[i])
				continue;
			else
				return FALSE;
		}
		return TRUE; /*Equal keys*/
	} else
		return FALSE;
}

/******************************************************************************/
/*Trie functions*/

TrieNode* make_trienode(NodeData data) {
	TrieNode *node = (TrieNode*) calloc(1, sizeof(TrieNode));
	for (int i = 0; i < ALPHABET_SIZE; i++)
		node->children[i] = NULL;
	node->isLeaf = FALSE;
	node->data = data;
	return node;
}

void free_trienode(TrieNode *node) {
	// Free the trienode sequence
	for (int i = 0; i < ALPHABET_SIZE; i++) {
		if (node->children[i] != NULL) {
			free_trienode(node->children[i]);
		} else {
			continue;
		}
	}
	free(node);
}

TrieNode* insert_trie(TrieNode *root, NodeData data) {
	TrieNode *temp = root;
	unsigned int int_aux;

	for (int i = 0; data.word[i] != '\0'; i++) {
		int_aux = letter2int(data.word[i]);
		if (temp->children[int_aux] == NULL) {
			// If the corresponding child doesn't exist, simply create that child
			temp->children[int_aux] = make_trienode(data);
			break;
		} else {
			temp = temp->children[int_aux];
		}
	}
	// At the end of the word, mark this node as the leaf node
	temp->isLeaf = TRUE;
	return root;
}

char* search_trie(TrieNode *root, char *word) {
	TrieNode *temp = root;
	unsigned int int_aux;
	char *keyAux = keygen(word);
	Boolean equals = keyCmp(temp->data.key, keyAux);
	if (equals)
		return temp->data.definition;

	/*Attempt starting first level*/
	for (int i = 0; word[i] != '\0'; i++) {
		int_aux = letter2int(word[i]);
		if (temp->children[int_aux] != NULL) {
			equals = keyCmp(temp->children[int_aux]->data.key, keyAux);
			if (equals)
				return temp->children[int_aux]->data.definition;
			else
				temp = temp->children[int_aux];
		} else {
			return NULL; /*Doesn't exist*/
		}
	}
	return NULL; /*Doesn't exist*/
}

/******************************************************************************/
/*Dictionary functions*/

Dictionary dict_create() {
	Dictionary instance = (Dictionary) malloc(sizeof(struct str_trie));
	instance->size = ZERO;
	instance->root = NULL;
	return instance;
}

void dict_set(Dictionary d, char *word, char *definition) {
	NodeData data;
	data.key = keygen(word);
	data.word = word;
	data.definition = definition;

	if (d->root == NULL)
		d->root = make_trienode(data);
	else
		insert_trie(d->root, data);

	d->size = d->size + 1;
}

char* dict_get(Dictionary d, char *word) {

	printf("\nSearching for %s: ", word);
	char *aux = search_trie(d->root, word);
	if (aux) {
		printf("\nFound!");
		printf("\nMeaning for word %s:", word);
		printf(" %s", aux);
	} else
		printf("Not Found\n");
	return aux;
}

unsigned int dict_size(Dictionary d) {
	return d->size;
}

void dict_destroy(Dictionary d) {
	free_trienode(d->root);
	free(d);
}

