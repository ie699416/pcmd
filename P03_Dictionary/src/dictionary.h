#ifndef DICTIONARY_H
#define DICTIONARY_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "type.h"

#define MAX_WORD_SIZE   40
#define MAX_DESC_SIZE  200

#define ALPHABET_SIZE 	28 		/* ABCDEFGHIJKLMNOPQRSTUVWXYZ�_ */
#define UNDERSCORE	  	27		/* Position 0 for '_' character */
#define ENIE	  		26		/* Position 0 for '_' character */
/******************************************************************************/

typedef struct str_dictionary {
	char *word;
	char *key;
	char *definition;
} NodeData;

typedef struct strNode {
	NodeData data;		 // <Word, Key, Definition>
	Boolean isLeaf; 	 // Leaf Flag
	struct strNode *children[ALPHABET_SIZE]; 		// children branches
} TrieNode;

struct str_trie {
	TrieNode *root;
	unsigned int size;
};

typedef struct str_trie *Dictionary;

/******************************************************************************/

/* Done */
Dictionary dict_create();

/* Done */
void dict_set(Dictionary, char *word, char *definition);

/* Done */
void dict_destroy(Dictionary);

/* Done */
unsigned int dict_size(Dictionary);

/* Done */
char* dict_get(Dictionary, char *word);

/* Done */
char** dict_keys(Dictionary);

#endif
