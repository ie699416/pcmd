/*
 * Type.h
 *
 *  Created on: Mar 26, 2020
 *      Author: administrador
 */

#ifndef TYPE_H_
#define TYPE_H_

#define FALSE		0
#define TRUE		1

#include <stdio.h>
#include <stdlib.h>

typedef void * Type;

typedef enum {false, true} Boolean;

Type Int(int);

Type Float(float);

Type Char(char);

Type String(char *);

#endif /* TYPE_H_ */
