

#include <stdio.h>
#include <stdlib.h>
#include "Type.h"
#include "Queue.h"

int main(void) {

	//Prueba de las funciones para crear instancias de Enteros, Reales, Caracteres y Cadenas
	//Reservan memoria dinámica, realizan un cast a tipo Type y retornan la dirección de memoria

	//Números enteros
	Type p=Int(3);
	printf("Address: %p \n", (int *)p);
	printf("Content: %d \n\n", *(int *)p);
	free(p);

	//Números reales
	p=Float(3.2);
	printf("Address: %p \n", (float *)p);
	printf("Content: %f \n\n", *(float *)p);
	free(p);

	//Caracteres
	p=Char('A');
	printf("Address: %p \n", (char *)p);
	printf("Content: %c \n\n", *(char *)p);
	free(p);

	//Cadenas de texto
	p=String("Hello World");
	printf("Address: %p \n", (char *)p);
	printf("Content: %s \n\n", (char *)p);
	free(p);

	printf("\n\tTesting Queue \n");
	Queue q1;
	q1=queue_create();
	for(int i=20;i<=30;i++){
		queue_offer(q1, Int(i));
	}

	printf("Size (q1): %d \n", queue_size(q1));
	printf("Last element in queue (q1): %d \n", *(int *)queue_peek(q1));


	while(queue_isEmpty(q1)!=TRUE){
		printf(" %d ", *(int *)queue_poll(q1));
	}

	queue_destroy(q1);
	printf("\nQueue (q1) has been destroyed ... \n");


	return 0;
}
