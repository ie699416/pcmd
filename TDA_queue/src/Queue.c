/*
 * Queue.c
 *
 *  Created on: 26/04/2016
 *      Author: IE699416
 */


#include "queue.h"
#include <stdlib.h>
#include <stdio.h>


//Definición de la estructura de datos requerida para construir Nodos
struct strNode{
  Type data;
  struct strNode *next;
};

typedef struct strNode *Node;

//Definición de la estructura de datos requerida para construir el contenedor Queue

struct strQueue{
  Node first;
  Node last;
  unsigned int size;
};

//Función para crear nuevos nodos e inicializa
Node create_node(Type data){
  Node new=(Node)malloc(sizeof(struct strNode));
  new->next=NULL;
  new->data=data;
  return new;
}

// Crea una nueva instancia del contenedor Queue con memoria dinámica
// y los inicializa con NULL, NULL y 0.
Queue queue_create(){
  Queue new=(Queue)malloc(sizeof(struct strQueue));
  new->last=NULL;
  new->first=NULL;
  new->size=0;
  return new;
}

// Retorna el tamaño de la fila
unsigned int queue_size(Queue q){
  if(q){
    return q->size;
  }
	return 0;
}

// ¿El primer elemento en la fila es nulo?
Boolean queue_isEmpty(Queue q){
  if(q){
    if(q->first==NULL)
      return TRUE;
    return FALSE;
  }
	return TRUE;
}

// Consulta quién está al frente de la fila (pero no elimina)
Type queue_peek(Queue q){
	if(q){
		//Existe la fila q
		if(q->size>0){
			//Existen elementos en la fila q
			return q->first->data;
		}
		else //No existen elementos en la fila q
			return NULL;
	}
	return NULL;    //No existe la fila q
}

// Se inserta un nuevo dato (colocarlo al final)
void  queue_offer(Queue q, Type d){
  if(q){

	  //Si existe la Fila
	Node new=create_node(d);

    if(queue_isEmpty(q)){
      //La fila no tiene elementos
    	q->first=new;
    	q->last=new;
    }
    else{
      //La Fila Si tiene elementos,
      //Agreguemos el nuevo elemento al final de la fila
    	q->last->next=new;
    	q->last=new;
    }
    //Actualiza el número de elementos almacenados en el contenedor
    q->size=q->size+1;

  }
}


// Atiende al elemento que está al frente (extrae de la fila y lo elimina)
Type queue_poll(Queue q){
	if(q){
		//Existe la fila q
		if(q->size>0){
			//Existen elementos en la fila q

			//Definimos dos apuntadores temporales para no perder las direcciones
			//en donde se encuentra el nodo first y el dato

			Type data_temp=q->first->data;
			Type node_temp=q->first;

			//Movemos el apuntador first
			q->first=q->first->next;

			//Liberamos memoria dinámica del nodo donde se encuentra el dato
			//que vamos a extraer de la fila
			free(node_temp);
			q->size=q->size-1;

			return data_temp;
		}
		else //No existen elementos en la fila q
			return NULL;
	}
	return NULL;    //No existe la fila q
}


// Elimina a todos los elementos almacenados y a la fila misma
void  queue_destroy(Queue q){
	while(q->first!=NULL){
		Type temp = queue_poll(q);
		free(temp);
	}
	free(q);
}
