
#ifndef QUEUE_H_
#define QUEUE_H_

#include "Type.h"
typedef struct strQueue* Queue;

Queue queue_create();

unsigned int queue_size(Queue);

Boolean queue_isEmpty(Queue);

void queue_destroy(Queue);

void queue_offer(Queue, Type);

Type queue_poll(Queue);

Type queue_peek(Queue);



#endif /* QUEUE_H_ */
