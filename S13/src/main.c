/*
 ============================================================================
 Name        : main.c
 Author      : ie699416
 Version     : 1.1
 Description : Main program file.
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(void) {
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);

	char path[260];	// Windows PATH limit is 260 characters;

	while (1) {
		printf("\n\tEnter file path:\n");
		scanf("%s", path);
		char *pathPtr = (char*) strdup(path);		// Variable path length.
		char *temp = pathPtr;

		while (*temp != '\0') {
			printf("%c", *temp);
			temp++;
		}

		printf("\n\n");
		FILE *f = fopen(pathPtr, "r");

		if (f == NULL) {
			printf("\n\tError: File could not be opened.\n");
			fclose(f);
		} else {
			fseek(f, 0, SEEK_END);
			long nBytes = ftell(f);
			rewind(f);
			printf("\n\tBytes : %ld\n", nBytes);

			char *text = (char*) malloc(nBytes * sizeof(char));
			int i = 0;
			while (i < nBytes) {
				text[i] = fgetc(f);
				printf("%c", text[i]);
				i++;
			}

			fclose(f);
			return 0;
		}
	}

	return 0;
}
