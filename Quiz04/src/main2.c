/*
 * main2.c
 *
 *  Created on: Feb 25, 2020
 *      Author: C20256
 */




#include <stdio.h>
#include <stdlib.h>

void mult(int*, int, float*);

typedef struct str_auto Auto;

int main() {
	int x[] = {6,7,3,10};
	int n = 4;
	float r = 0;


	mult(x,n,&r );

	printf("%.2f", r);

}

void mult(int *A, int n, float *promedio) {
	int p = 0;
	for(int i = 0; i<n;i++){
		p = p + (*A);
		A++;
	}

	* promedio = (int) ((float) p / (float) n);

}


