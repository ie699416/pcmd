///*
// * main.c
// *
// *  Created on: Feb 24, 2020
// *      Author: C20256
// */
//
//#include <stdio.h>
//#include <stdlib.h>
//
//#define TRUE	1
//#define FALSE	0
//
//struct str_array {
//	int *value;				// Pointer for dynamic array memory allocation
//	unsigned int size;		// Number of elements in the dynamic array
//};
//
//typedef struct str_array Array;
//
//typedef enum {
//	False, True
//} boolean;
//
//// Par�metro: tama�o del arreglo
//Array* newArray(unsigned int);
//
//// Par�metros: Arreglo, �ndice, valor
//boolean setValue(Array*, unsigned int, int);
//
//// Par�metro: Arreglo que se quiere destruir
//void destroyArray(Array*);
//
//// Par�metros: Arreglo del que se va a imprimir su tama�o
//unsigned int array_size(Array*);
//
//int main() {
//
//	Array *myArray;
//
//	//Ejemplo de uso de las funciones
//	myArray = newArray(10);    //Crea un arreglo para almacenar 10 enteros
//	printf("\n\tTama�o del arreglo: %u", array_size(myArray));
//	boolean estatus;
//	estatus = setValue(myArray, 10, 5); //Almacena 10 en la posici�n 5 dentro de myArray
//	if (estatus)
//		printf("\n\tEl valor se asign� de manera correcta");
//	else
//		printf("\n\tFuera de alcance.");
//
//	destroyArray(myArray);   //Libera la memoria din�mica
//
//	return 0;
//
//}
//
//// Par�metro: tama�o del arreglo
//Array* newArray(unsigned int size) {
//	Array *array = (Array*) malloc(sizeof(array));
//	array->size = size;
//	array->value = (int*) malloc(size * sizeof(int));
//	return array;
//}
//
//// Par�metros: Arreglo, �ndice, valor
//boolean setValue(Array *array, unsigned int pos, int value) {
//	if (array->size>=pos) {
//		array->value[pos] = value;
//		return TRUE;
//	} else
//		return FALSE;
//}
//
//// Par�metro: Arreglo que se quiere destruir
//void destroyArray(Array *array) {
//	free(array->value);
//	free(array);
//}
//
//// Par�metros: Arreglo del que se va a imprimir su tama�o
//unsigned int array_size(Array *array) {
//	return array->size;
//}
