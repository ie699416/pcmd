///*
// * 9.c
// *
// *  Created on: Feb 24, 2020
// *      Author: C20256
// */
//
//#define NA 5
//
//#include <stdio.h>
//#include <stdlib.h>
//
//struct str_auto {
//	char marca[20];
//	char modelo[10];
//	char placa[7];
//	float kilometraje;
//};
//
//typedef struct str_auto Auto;
//
//int main() {
//	setvbuf(stdout, NULL, _IONBF, 0);
//	setvbuf(stderr, NULL, _IONBF, 0);
//	Auto autos[NA];
//	Auto *pauto = &autos[0];
//	//Capturar datos de los autos, utilizando el apuntador pauto
//
//	for (int i = 0; i < NA; i++) {
//		printf("Datos del auto #00%d \n", i + 1);
//		printf("Marca: ");
//		scanf("%s", (*pauto).marca);
//		printf("Modelo: ");
//		scanf("%s", (*pauto).modelo);
//		printf("Placa: ");
//		scanf("%s", (*pauto).placa);
//		printf("Kilometraje: ");
//		scanf("%f", &(*pauto).kilometraje);
//		pauto++;
//	}
//	pauto = &autos[NA-1];    //Hacer que apunte al �ltimo elemento del arreglo
//	printf("\nMarca:       %s \n", (*pauto).marca);
//	printf("Modelo:      %s \n", (*pauto).modelo);
//	printf("Placa:       %s \n", pauto->placa);
//	printf("Kilometraje: %.2f \n", (*pauto).kilometraje);
//	return 0;
//}
