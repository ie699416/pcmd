//
//  Type.h
//  Set_xcode
//
//  Created by JFCA on 23/04/20.
//  Copyright © 2020 personal. All rights reserved.
//

#ifndef TYPE_H_
#define TYPE_H_

typedef enum {False, True} boolean;

typedef void * Type;

Type Int(int);

Type Float(float);

Type Char(char);

Type String(char *);

int Type2Int(Type);

#endif /* TYPE_H_ */
