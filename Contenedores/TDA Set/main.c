//
//  main.c
//  Set_xcode
//
//  Created by JFCA on 23/04/20.
//  Copyright © 2020 personal. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "Type.h"
#include "Set.h"

int compareInt(Type k1, Type k2);

void PrintIntKey(Type key);

int main(){
    Set s1;
    s1=set_create(compareInt);
    printf("# de elementos en el contenedor: %d \n", set_size(s1));
    printf("Agregando elementos al contenedor Set ... \n");
    set_add(s1, Int(5));
    set_add(s1, Int(3));
    set_add(s1, Int(4));
    set_add(s1, Int(2));
    set_add(s1, Int(1));
    set_add(s1, Int(8));
    set_add(s1, Int(9));
    set_add(s1, Int(6));
    set_add(s1, Int(7));
    set_add(s1, Int(10));
    set_add(s1, Int(0));
    
    printf("# de elementos en el contenedor: %d \n", set_size(s1));
    printf("Contenido del contenedor (inorden): \n");
    set_print(s1, PrintIntKey);
    
    printf("\nEliminando un elemento del contendor Set ... \n");
    set_remove(s1, Int(1));
    
    printf("# de elementos en el contenedor: %d \n", set_size(s1));

    printf("Contenido del contenedor (inorden): \n");
    set_print(s1, PrintIntKey);
    printf("\nFin del programa \n");
    return 0;
}

int compareInt(Type k1, Type k2){
    int *d1=(int *)k1;
    int *d2=(int *)k2;
    return *d1-*d2;
}

void PrintIntKey(Type key){
    int *k=(int *)key;
    printf(" %d ", *k);
}
