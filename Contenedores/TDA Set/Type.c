//
//  Type.c
//  Set_xcode
//
//  Created by JFCA on 23/04/20.
//  Copyright © 2020 personal. All rights reserved.
//

#include <stdlib.h>
#include "Type.h"
#include <string.h>

Type Int(int n){
    int * new=(int *)malloc(sizeof(int));
    *new=n;
    return (Type)new;
}

Type Float(float f){
    float * new=(float *)malloc(sizeof(float));
    *new=f;
    return (Type)new;
}

Type Char(char c){
    char * new=(char *)malloc(sizeof(char));
    *new=c;
    return (Type)new;
}

Type String(char *s){
    char * new=strdup(s);
    return (Type)new;
}

int Type2Int(Type data){
    int num=*(int *)data;
    return num;
}

