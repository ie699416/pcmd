//
//  Set.h
//  Set_xcode
//
//  Created by JFCA on 23/04/20.
//  Copyright © 2020 personal. All rights reserved.
//

#ifndef SET_H_
#define SET_H_

#include "Type.h"

typedef struct strSet *Set;

typedef int (*CMP)(Type, Type);

typedef void (*PrintKey)(Type);

Set set_create(CMP);

unsigned int set_size(Set);

boolean set_add(Set, Type);

boolean set_contains(Set, Type key);

boolean set_remove(Set s, Type key);

void set_print(Set s, PrintKey pk);

void set_destroy(Set s);

#endif /* SET_H_ */
