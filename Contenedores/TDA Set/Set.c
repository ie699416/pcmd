//
//  Set.c
//  Set_xcode
//
//  Created by JFCA on 23/04/20.
//  Copyright © 2020 personal. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "Type.h"
#include "Set.h"

// Definición de la estructura de datos que permitirá crear nodos del árbol (raiz, internos y hojas)
struct strNode{
    Type data;
    struct strNode *l;
    struct strNode *r;
};

typedef struct strNode *Node;

struct strParentNode{
    Node parent;
    Node current;
};

typedef struct strParentNode ParentNode;

// Definición de la estructura de datos que permitirá crear nuevas instancias del contenedor Set
struct strSet{
    int size;        //Número de elementos que tiene el contenedor
    Node root;        //Apuntador al inicio del árbol
    CMP datacmp;        //Apuntador a función que sabe cómo comparar claves
                       //(se requiere para buscar, agregar y eliminar elementos en el conjunto)
};

Set set_create(CMP datacmp){
    Set new=NULL;
    // Escribe tu código aquí para reservar memoria dinámica para un nuevo Set e inicializar los valores de size, root y datacmp.
    new=(Set)malloc(sizeof(struct strSet));
    if(new!=NULL){
        new->size=0;
        new->root=NULL;
        new->datacmp=datacmp;
    }
    return new;
}

unsigned int set_size(Set s){
    // Escribe tu código aquí para retornar el número de elementos que tiene almacenado el contenedor
    if(s)
        return s->size;
    else
        return 0;
}

Node createNode(Type data){
    Node new=(Node)malloc(sizeof(struct strNode));
    new->data=data;
    new->l=NULL;
    new->r=NULL;
    return new;
}

boolean set_add_node(Set s, Node root, Type data){
    boolean status=False;
    int r=s->datacmp(data, root->data);
    if(r>0){
        if(root->r)
            status=set_add_node(s, root->r, data);
        else{
            root->r=createNode(data);
            status=True;
        }
    }
    else{
        if(r<0){
            if(root->l)
                status=set_add_node(s, root->l, data);
            else{
                root->l=createNode(data);
                status=True;
            }
        }
    }
    return status;
}

boolean set_add(Set s, Type data){
    boolean status=False;
    // Escribe tu código aquí para agregar un nuevo elemento al conjunto,
    // si ya existe data no se agrega y se retorna False,
    //en otro caso, agregar el nuevo nodo y retornar True

    //Existen dos casos
    //Caso 1: Root == NULL
    if(s->root==NULL){
        s->root=createNode(data);
        s->size+=1;
        status=True;
    }
    else
    {
        //Caso 2: Root !=NULL
        status=set_add_node(s, s->root, data);
        if (status)
            s->size+=1;
    }
    return status;
}

boolean set_search_node(Set s, Node root, Type data){
    boolean status=False;
    int r;
    r=s->datacmp(data, root->data);
    if(r==0)
        status=True;
    else{
        if(r<0){
        //Izquierda
            if(root->l){
                status=set_search_node(s, root->l,data);
            }
        }
        else{
        //Derecha
            if(root->r){
                status=set_search_node(s, root->r,data);
            }
        }
    }
    return status;
}

boolean set_contains(Set s, Type data){
    boolean status=False;
    int r;
    if(s->root){
        r=s->datacmp(data, s->root->data);
        if(r==0)
            status=True;
        else{
            //¿izquierda o derecha?
            if(r<0){
            //Izquierda
                if(s->root->l){
                    status=set_search_node(s, s->root->l,data);
                }
            }else{
            //Derecha
                if(s->root->r){
                    //Invocar función recursiva de búsqueda
                    status=set_search_node(s, s->root->r,data);
                }
            }
        }
    }
    return status;
}

//Se utiliza para la operación: remove
ParentNode searchNode(Set s, Node parent, Node current, Type data){
    ParentNode target;
    target.current=NULL;
    target.parent=NULL;
    int r;
    r=s->datacmp(data, current->data);
    if(r==0){
        target.current=current;
        target.parent=parent;
    }
    else{
        if(r<0){
            //Izquierda
            if(current->l)
                target=searchNode(s, current, current->l, data);
        }
        else{
            //Derecha
            if(current->r)
                target=searchNode(s, current, current->r, data);
        }
    }
    return target;
}

//Se utiliza para la operación: remove
ParentNode searchNode_MaxLeft(Set s, Node parent, Node current, Type data){
    ParentNode target;
    target.current=current;
    target.parent=parent;
    while(target.current->r){
        target.parent=current;
        target.current=target.parent->r;
    }
    return target;
}

//Se utiliza para la operación: remove
void swapKV(Node target, Node maxLeft){
    Type tmpData;
    tmpData=target->data;
    target->data=maxLeft->data;
    maxLeft->data=tmpData;
}

boolean set_remove(Set s, Type data){
    boolean status=False;
    ParentNode target;
    // Escribe tu código aquí para buscar el elemento data, si lo encuentra eliminar y retornar True, en otro caso retornar False
    //Caso 1: No existe el elemento a eliminar
    if(s->root){
        target=searchNode(s, NULL, s->root, data);
        if(target.current){
            //Si se encontró el objetivo
            printf("<< Target >>\n");
            if(target.parent)
                printf("Parent: %d \n", *((int *)target.parent->data));
            else
                printf("Parent: NULL \n");
            printf("Node: %d \n", *((int *)target.current->data));
            if(!((target.current->l)||(target.current->r))){
                //Caso 2: Se trata de un nodo hoja
                printf("Se trata de un nodo hoja \n");
                if(target.parent->l==target.current)
                {   //Es un hijo izquierdo?
                    target.parent->l=NULL;
                }
                else{
                    //Es un hijo derecho?
                    target.parent->r=NULL;
                }
                free(target.current);
                s->size-=1;
            }else{
                //Caso 3: El nodo tiene solamente un hijo
                if((target.current->l)&&(!target.current->r)){
                    //Tiene un hijo izquierdo
                    printf("Tiene solamente el hijo izquierdo \n");
                    if(target.parent->l==target.current)
                    {   //El target es un hijo izquierdo?
                        printf("El objetivo es un nodo izquierdo \n");
                        target.parent->l=target.current->l;
                    }
                    else{
                        //Es un hijo derecho?
                        printf("El objetivo es un nodo derecho \n");
                        target.parent->r=target.current->l;
                    }
                    free(target.current);
                    s->size-=1;
                }
                else{
                    if((target.current->r)&&(!target.current->l)){
                        //Tiene un hijo derecho
                        printf("Tiene solamente el hijo derecho \n");
                        if(target.parent->l==target.current)
                        {   //El target es un hijo izquierdo?
                            printf("El objetivo es un nodo izquierdo \n");
                            target.parent->l=target.current->r;
                        }
                        else{
                            //El target es un hijo derecho?
                            printf("El objetivo es un nodo derecho \n");
                            target.parent->r=target.current->r;
                        }
                        free(target.current);
                        s->size-=1;
                    }
                    else{
                        //Caso 4: El nodo tiene dos hijos
                        printf("Tiene dos hijos  \n");
                        //Utilizaremos la estrategia de buscar el elemento mayor del subárbol izquierdo
                        ParentNode maxLeft=searchNode_MaxLeft(s, target.current, target.current->l, data);
                        printf("<< maxLeft >>\n");
                        printf("Parent: %d \n", *((int *)maxLeft.parent->data));
                        printf("Node: %d \n", *((int *)maxLeft.current->data));
                        swapKV(target.current, maxLeft.current);
                        if(maxLeft.current->l)
                            maxLeft.parent->r=maxLeft.current->l;
                        free(maxLeft.current);
                        s->size-=1;
                    }
                }
            }
        }
    }
    return status;
}

void printNodes(Node current, PrintKey pk){
    if(current){
        printNodes(current->l, pk);
        pk(current->data);
        printNodes(current->r, pk);
    }
}

void set_print(Set s, PrintKey pk){
    printNodes(s->root, pk);
}

void set_destroy(Set s){
    //Escribe tu código para eliminar elementos del conjunto
    //mientras su número de elementos sea mayor a 0
      
    
}
