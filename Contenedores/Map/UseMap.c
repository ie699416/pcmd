
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Type.h"
#include "Map.h"

unsigned int hashFunction(Type key, unsigned int size);
int cmpData(Type d1, Type d2);

typedef struct{
	float x, y, z;
}Vertex;

Vertex * newVertex(float x, float y, float z);

int main(){
	Map m1=map_create(50, 0.9, 0.5,hashFunction,cmpData);

	unsigned int k;
	Vertex *v1=newVertex(1.0, 3.0 ,4.5);
	map_set(m1,Int(0), (Type)v1);

	Vertex *v2=newVertex(2.0, 3.0 ,4.5);
	map_set(m1,Int(1), (Type)v2);

	Vertex *v3=newVertex(3.0, 3.0 ,4.5);
	map_set(m1,Int(4), (Type)v3);

	k=1;
	Vertex *tmp=(Vertex *)map_get(m1, (Type)&k);
	printf("%f , %f , %f \n", tmp->x, tmp->y, tmp->z);

	printf("Fin del programa ...");
	return 0;
}

unsigned int hashFunction(Type key, unsigned int size){
	unsigned int *k= (unsigned int *)key;
	return *k%size;
}

int cmpData(Type d1, Type d2){
	int *v1=(int *)d1;
	int *v2=(int *)d2;
	return *v1-*v2;
}

Vertex * newVertex(float x, float y, float z){
	Vertex *v=(Vertex *)malloc(sizeof(Vertex));
	v->x=x;
	v->y=y;
	v->z=z;
	return v;
}
