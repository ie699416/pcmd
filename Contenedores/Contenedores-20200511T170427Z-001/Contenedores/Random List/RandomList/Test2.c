#include <RandomList.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef enum {
	Start, Add, Remove, Exit
} Menu;
typedef enum {
	Next, Previous
} Play;
typedef enum {
	No, Yes
} Answer;

char * getMenu(Menu op) {
	switch (op) {
	case Start:
		return "Start";
	case Add:
		return "Add";
	case Remove:
		return "Remove";
	case Exit:
		return "Exit";

	default:
		return 0;

	}
}
char * getPlay(Menu op) {
	switch (op) {
	case Next:
		return "Next";
	case Previous:
		return "Previous";

	default:
		return 0;

	}
}

char * getAns(Answer Ans) {
	switch (Ans) {
	case Yes:
		return "Yes";
	case No:
		return "No";

	default:
		return "No";

	}
}

int main() {
	srand(time(NULL));
	setvbuf(stdout, NULL, _IONBF, 0);
	int i = 0;

	Menu Options = 0;
	int op = 0;
	Answer Ans = 0;
	int an = 0;
	Play Player = 0;
	int pl = 0;

	char text[] = "hola";

	RandomList r = rlist_create();
	rlist_addTray(r, "Uno");
	rlist_addTray(r, "Dos");
	rlist_addTray(r, "Tres");
	rlist_addTray(r, "Cuatro");
//	rlist_peek(r);
//	printf("%s\n", (char*) rlist_peek(r));

//	rlist_start(r);





	while (1) {
		printf("Please choose an option:   \n\n");
		for (i = 0; i < 4; i++) {
			Options = i;
			printf(" [%d] %s \n", i, getMenu(Options));

		}

		scanf("%d", &op);
		Options = (Menu) op;

		switch (Options) {

		case 0: {
			printf("You've selected start!  \n");

			if (list_size(r) == 0) {
				puts(
						"It seems like you do not have any 'Songs' on the PlayList, Would you like to add some now?\n");
				puts(" \n [0] No\n [1] Yes\n");

				scanf("%d", &an);
				Ans = (Answer) an;

				switch (Ans) {
				case 0:
					break;
				case 1: {
					puts(
							"Please write down on console the name of the 'Song'\n");
//					puts(text);
					scanf("%s", text);

					rlist_addTray(r, text);
					printf("Successfully added: '%s' on the PlayList\n", text);
					continue;
				}
				}
			} else if (list_size(r) != 0) {
				Type t = rlist_start(r);
				printf("Now playing: %s", (char*) t);

				puts(
						"It seems like you do not have any 'Songs' on the PlayList, Would you like to add some now?\n");
				puts(" \n [0] No\n [1] Yes\n");

				for (i = 0; i < 4; i++) {
					Options = i;
					printf(" [%d] %s \n", i, getMenu(Options));

				}

				scanf("%d", &pl);
				Player = (Play) pl;

				switch (Player) {
				case 0: {
					rlist_next(r);

				}
				case 1: {
					rlist_previous(r);
				}
				}

				continue;

			}

			continue;

		}
		case 1: {
			puts("You have Selected 'Add'\n");
			puts("Please write down on console the name of the 'Song'\n");
			scanf("%s", text);
			printf("%s  \n", text);
			rlist_addTray(r, text);
			printf("Succesfully added: '%s' on the PlayList\n", text);
			continue;
		}
		case 2: {
			printf("You've selected Remove\n");
			continue;
		}

		case 3: {
			puts(" Would you like to exit? \n \n [0] Exit \n [1] Continue;");
			scanf("%d", &an);
			Ans = (Answer) op;
			switch (Ans) {
			case 0:
				return 0;
			case 1:
				continue;
			}
			continue;
		}

		default: {
			puts(
					"You haven't choose any option \n \n Would you like to continue? \n \n [0] Continue \n [1] Exit;");
			scanf("%d", &an);
			Ans = (Answer) op;
			switch (Ans) {
			case 0:
				continue;
			case 1:
				return 0;

			}

		}
			break;
		}
		puts("\n");

	}

	return 0;

}
