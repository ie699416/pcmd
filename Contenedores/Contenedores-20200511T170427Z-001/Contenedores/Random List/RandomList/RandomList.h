/*
 * RandomList.h
 *
 *  Created on: 09/05/2016
 *      Author: IE699416
 */

#ifndef RANDOMLIST_H_
#define RANDOMLIST_H_

typedef struct strRandomList * RandomList;
typedef void * Type;
typedef struct strList* List;
typedef enum {
	FALSE, TRUE
} Bool;

RandomList rlist_create();
void rlist_destroy(RandomList);
int rlist_size(RandomList);
void rlist_addTray(RandomList, Type);
void rlist_addRandom(RandomList);
Type rlist_next(RandomList);
Type rlist_previous(RandomList);
Type rlist_peek(RandomList);
Type rlist_poll(RandomList);

Type rlist_random(RandomList);
Type rlist_start(RandomList);
int list_size(RandomList);
void Shuffle(RandomList);
void Repeat(RandomList);
void rlist_display(RandomList);
void list_display(RandomList);
void rlist_remove(RandomList, char *);

#endif /* RANDOMLIST_H_ */
