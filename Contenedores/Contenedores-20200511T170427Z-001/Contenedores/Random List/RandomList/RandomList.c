#include "randomlist.h"
#include <stdio.h>
#include <stdlib.h>
//typedef enum {	Start, Add, Remove, Next, Previous, Stop, Exit} Menu;

struct strNode {
	Type data;
	struct strNode *prior, *next;
	Bool repeat;
};

typedef struct strNode* Node;

struct strRandomList {
	Node first, last, current;
	int size;
	Bool shuffle;
	Bool repeat;
	List tray; // Tray on which all the inputs manually tray will be offered for the Random List to make a choice.

};

struct strList {
	Node first, last, current;
	int size; // Tray on which all the inputs manually tray will be offered for the Random List to make a choice.

};

RandomList rlist_create() {          //RandomList is created with Dinamic Memory
	RandomList r = (RandomList) malloc(sizeof(struct strRandomList));
	r->first = r->last = r->current = NULL;    //First paths initialized as NULL
	r->shuffle = r->repeat = 1;        // Shuffle & Repeat INITIALIZED AS ACTIVE
	r->size = 0;

	r->tray = (List) malloc(sizeof(struct strList)); //Tray created also with Dinamic Memory
	r->tray->first = r->tray->last = r->tray->current = NULL; // Also all Values from my tray will be null in the meanwhile
	r->tray->size = 0;

	return r;
}

void rlist_destroy(RandomList r) {

}

void rlist_addTray(RandomList r, Type t) {
	Node n = (Node) malloc(sizeof(struct strNode));
	n->data = t;
	n->next = NULL;

	if (r->tray->size == 0) {
		n->prior = NULL;
		r->tray->first = r->tray->last = r->tray->current= n;
	}

	else {
		n->prior = r->tray->last;
		r->tray->current->next = n;
		r->tray->current = n;
		r->tray->last = n;
	}
	r->tray->size++;

}

void rlist_addRandom(RandomList r) {
	Node n = (Node) malloc(sizeof(struct strNode));
	Type t = rlist_peek(r);
	n->data = t;
	n->next = NULL;

	if (r->size == 0) {
		n->prior = NULL;
		r->first = r->last = r->current = n;

	} else {
		n->prior = r->last;
		r->current->next = n;
		r->current = n;
		r->last = n;

	}

	r->size++;

}

void rlist_add(RandomList r, Type t) {
	Node n = (Node) malloc(sizeof(struct strNode));
	n->data = t;
	n->next = NULL;

	if (r->size == 0) {
		n->prior = NULL;
		r->first = r->last = r->current = n;

	} else {
		n->prior = r->last;
		r->current->next = n;
		r->current = n;
		r->last = n;

	}

	r->size++;

}

Type rlist_next(RandomList r) {
	if (r->current->next != NULL) {
		r->current = r->current->next;
		return r->current->data;
	}

	rlist_addRandom(r);
	return r->current->data;
	/*
	 //  if (r->shuffle == 1 && r->repeat == 1) {

	 //	   }
	 //
	 //  else if (r->shuffle == 1 && r->repeat == 0) {
	 //
	 //  }
	 //
	 //  else if (r->shuffle == 0 && r->repeat == 0) {
	 //
	 //  }
	 //
	 //  else if (r->shuffle == 0 && r->repeat == 1) {
	 //
	 //  }
	 //
	 //  return NULL;
	 *
	 */
}

Type rlist_previous(RandomList r) {
	if (r->current->prior == NULL) {
		return 0;
	}
	return r->current->prior->data;
}


Type rlist_peek(RandomList r) {

	RandomList aux = r;
	aux->current = aux->first;

	int i = 1;
	int peek = rand() % rlist_size(r) + 1;
	printf("%d\n", peek);

	while (i < peek) {
		aux->current = aux->current->next;
		i++;
	}

	printf("%s\n", (char*) aux->current->data);
	return 0;
}

Type rlist_poll(RandomList r) {

	return 0;
}

Type rlist_random(RandomList r) {

	return 0;
}

int rlist_size(RandomList r) {

	return r->size;
}

Type rlist_start(RandomList r) {
	rlist_addRandom(r);
	return r->current->data;

}

int list_size(RandomList r) {
	return r->tray->size;
}

void Shuffle(RandomList r) {
	if (r->shuffle == 0) {
		r->shuffle = 1;
	} else {
		r->shuffle = 0;

	}

}

void Repeat(RandomList r) {
	if (r->repeat == 0) {
		r->repeat = 1;
		r->last->next = r->first;
	} else {
		r->repeat = 0;
		r->last->next = NULL;

	}

}

void rlist_display(RandomList r) {
	RandomList temp = r;
	temp->current = r->first;

	while (temp->current != NULL) {

		printf("%s\n", (char*) temp->current->data);
		temp->current = temp->current->next;

	}
	temp->current = r->first;

}

void list_display(RandomList r) {
	RandomList temp = r;
	temp->current = temp->tray->first;

	while (temp->current != NULL) {

		printf("%s\n", (char*) temp->current->data);
		temp->current = temp->current->next;

	}
	temp->current = r->first;

}

void rlist_remove(RandomList r, char * s) {
	RandomList temp = r;

	temp->tray->current = temp->tray->first;

	if (temp->tray->current != NULL) {
		while (temp->tray->current->data != s) {
			temp->tray->current = temp->tray->current->next;

		}
		temp->tray->current = temp->tray->current->prior;
	}

	if (temp->tray->current->data == s) {
		temp->tray->current->prior->next = temp->tray->current->next;
		//		free(temp->tray->current);
		r->tray = temp->tray;

		printf("Se ha podido remover %s exitosamente", s);
	} else {
		printf("No se ha podido remover %s ya que no estaba en la lista", s);
	}

}
//
//void list_destroy(List l) {
//  Node aux = l->first;
//  Node aux2;
//  while (aux != NULL) {
//    aux2 = aux->next;
//    free(aux);
//    aux = aux2;
//  }
//
//  free(l);
//}

