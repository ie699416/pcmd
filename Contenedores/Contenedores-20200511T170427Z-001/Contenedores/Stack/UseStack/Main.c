/*
 * Main.c
 *
 *  Created on: 06/04/2016
 *      Author: hpiza
 */
#include <stdio.h>
#include <Stack.h>

int main() {
	Stack miPrimerPila = stack_create();
	stack_push(miPrimerPila, "uno");
	stack_push(miPrimerPila, "dos");
	stack_push(miPrimerPila, "tres");
//	S� se puede a�adir "uno" porque es un char* y se guarda como void*

//	stack_push(miPrimerPila, 4);
//	No se pudo a�adir porque 4 es un int y se esperaba un int*
//	int i = 65;
//	stack_push(miPrimerPila, &i);

	char* c = (char*) stack_top(miPrimerPila);
	printf("%s\n", c);
	printf("%d\n", stack_size(miPrimerPila));

	c = (char*) stack_pop(miPrimerPila);
	printf("%s\n", c);
	printf("%d\n", stack_size(miPrimerPila));

	c = (char*) stack_pop(miPrimerPila);
	printf("%s\n", c);
	printf("%d\n", stack_size(miPrimerPila));

	c = (char*) stack_pop(miPrimerPila);
	printf("%s\n", c);
	printf("%d\n", stack_size(miPrimerPila));

	stack_destroy(miPrimerPila);
	return 0;
}
