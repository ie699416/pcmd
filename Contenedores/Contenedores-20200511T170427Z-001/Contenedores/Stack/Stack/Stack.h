/*
 * Stack.h
 *
 *  Created on: 06/04/2016
 *      Author: hpiza
 */

#ifndef STACK_H_
#define STACK_H_

typedef enum {FALSE, TRUE} Bool;
typedef void* Type;
typedef struct strStack* Stack;

Stack stack_create();
void  stack_destroy(Stack);
void  stack_push(Stack, Type);
int   stack_size(Stack);
Bool  stack_isEmpty(Stack);
Type  stack_top(Stack);
Type  stack_pop(Stack);

#endif /* STACK_H_ */
