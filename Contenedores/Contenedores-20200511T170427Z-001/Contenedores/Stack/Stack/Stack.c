/*
 * Stack.c
 *
 *  Created on: 06/04/2016
 *      Author: hpiza
 */
#include "Stack.h"
#include <stdlib.h>

struct strNode {
	Type data;
	struct strNode* prior;
};

typedef struct strNode* Node;

struct strStack {
	Node top;
	int size;
};

Stack stack_create() {
	Stack s = (Stack) malloc(sizeof(struct strStack));
	s->size = 0;
	s->top  = NULL;
	return s;
}

void  stack_destroy(Stack s) {
	Node current = s->top;
	if(!stack_isEmpty(s)) {
		Node prior;
		while(current->prior != NULL) {
			prior = current->prior;
			free(current);
			current = prior;
		}
		free(current);
	}
	free(s);
}

void  stack_push(Stack s, Type t) {
	Node n   = (Node) malloc(sizeof(struct strNode));
	n->data  = t;
	if(s->size == 0) {
		n->prior = NULL;
		s->top = n;
	} else {
		n->prior = s->top;
		s->top   = n;
	}
	s->size ++;
}

int   stack_size(Stack s) {
	return s->size;
}

Bool  stack_isEmpty(Stack s) {
//	if(s->top == NULL) return TRUE;
//	else return FALSE;

	return s->top == NULL;
//	return s->size == 0;
}

Type  stack_top(Stack s) {
	if(stack_isEmpty(s)) return NULL;

//	Node top = s->top;
//	Type data = top->data;
//	return data;
	return s->top->data;
}

Type  stack_pop(Stack s) {
	if(stack_isEmpty(s)) return NULL;
	Node top  = s->top;
	Type data = top->data;
	s->top    = top->prior;
	free(top);
	s->size --;
	return data;
}
