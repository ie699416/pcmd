/*
 * Queue.c
 *
 *  Created on: 11/04/2016
 *      Author: Iv�n
 */
#include "Queue.h"
#include <stdlib.h>
#include <stdio.h>

struct strNode {
	Type data;
	struct strNode* next;
};

typedef struct strNode* Node;

struct strQueue {
	Node first;
	Node last;
	unsigned int size;
};

Queue queue_create() {
	Queue q  = (Queue) malloc(sizeof(struct strQueue));
	q->first = NULL;
	q->last  = NULL;
	q->size  = 0;
	return q;
}

void queue_destroy(Queue q) {
	if(q->size > 0) {
		Node n;
		n = q->first;
		while(n->next != NULL){
			q->first = q->first->next;
			printf("Se elimina: %s\n", (char*) n->data);
			free(n);
			n = q->first;
		}
		printf("Se elimina: %s\n", (char*) n->data);
		free(n);
	}
	free(q);
}

void queue_offer(Queue q, Type t) {
	Node n = (Node) malloc(sizeof(struct strNode));
	n->data = t;
	n->next = NULL;
	if(q->size == 0) {
//		q->first = n;
//		q->last  = n;
		q->first = q->last = n;
	} else {
//		Node last  = q->last;
//		last->next = n;
		q->last->next = n;
		q->last       = n;
	}
	q->size ++;
}

Type queue_poll(Queue q) {
	Node f;
	Type d;
	if(q->size == 0){
		return NULL;
	}
	else{
		f = q->first;
		d = f->data;
		q->first = q->first->next;
		q->size--;
		free(f);
		return d;
	}
}

Type queue_peek(Queue q) {
	if(q->size == 0) {
		return NULL;
	} else {
		return q->first->data;
	}
}

int  queue_size(Queue q) {
	return q->size;
}

