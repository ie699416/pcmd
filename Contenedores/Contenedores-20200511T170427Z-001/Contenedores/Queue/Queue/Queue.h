/*
 * Queue.h
 *
 *  Created on: 11/04/2016
 *      Author: Iv�n
 */

#ifndef QUEUE_H_
#define QUEUE_H_

typedef void* Type;
typedef struct strQueue* Queue;

Queue queue_create();
void queue_destroy(Queue);
void queue_offer(Queue, Type);
Type queue_poll(Queue);
Type queue_peek(Queue);
int  queue_size(Queue);

#endif /* QUEUE_H_ */
