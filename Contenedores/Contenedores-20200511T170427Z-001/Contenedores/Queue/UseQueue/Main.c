/*
 * Main.c
 *
 *  Created on: 11/04/2016
 *      Author: Iv�n
 */
#include <Queue.h>
#include <stdio.h>

int main() {
	char* c;
	Queue q = queue_create();
	queue_offer(q, "Maritza");
	queue_offer(q, "Gaspar");
	c = queue_peek(q);
	puts(c);

	queue_offer(q, "Fregoso");
	queue_offer(q, "Z�rate^2");
	c = queue_peek(q);
	puts(c);

	c = queue_poll(q);
	puts(c);
	c = queue_poll(q);
	puts(c);
	c = queue_poll(q);
	puts(c);

	printf("%d\n", queue_size(q));
	queue_offer(q, "Magui");
	printf("%d\n", queue_size(q));
	queue_offer(q, "Amaya");
	printf("%d\n", queue_size(q));

//	c = queue_poll(q);
//	puts(c);
//	c = queue_poll(q);
//	puts(c);
//	c = queue_poll(q);
//	puts(c);

	queue_destroy(q);
	return 0;
}

