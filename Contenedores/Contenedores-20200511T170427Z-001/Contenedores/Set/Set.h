/*
 * Set.h
 *
 *  Created on: 20/04/2016
 *      Author: hpiza
 */

#ifndef SET_H_
#define SET_H_

typedef enum {FALSE, TRUE} Bool;
typedef void* Type;
typedef struct strSet* Set;
typedef int (*CompareFunc)(Type, Type);

Set set_create(CompareFunc);
Bool set_add(Set, Type);
int set_size(Set);
Bool set_contains(Set, Type);
void set_print(Set, void (*printFunc)(Type t, int spaces));
void set_destroy(Set);
Bool set_remove(Set, Type);

#endif /* SET_H_ */
