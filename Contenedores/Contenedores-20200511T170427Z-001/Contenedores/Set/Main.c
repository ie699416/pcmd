/*
 * Main.c
 *
 *  Created on: 20/04/2016
 *      Author: hpiza
 */
#include <Set.h>
#include <stdio.h>

int compareInts(Type t1, Type t2) {
//	int* i1 = (int*) t1;
//	int* i2 = (int*) t2;
//	return *i1 - *i2;
	return *((int*) t1) - *((int*) t2);
}

void printInt(Type t, int spaces) {
	int* i = (int*) t;
	while(spaces > 0) {
		printf("...");
		spaces --;
	}
	printf("%d\n", *i);
}

int main() {
	int array[] = { 5, 3, 8, 2, 9 };
	Set s = set_create(compareInts);
	int N = sizeof(array) / sizeof(int);
	int i;
	for(i = 0; i < N; i ++) set_add(s, &(array[i]));
	set_print(s, printInt);
	printf("----------\n");
	set_remove(s, &(array[0]));
	set_print(s, printInt);
	printf("----------\n");

	set_destroy(s);
	return 0;
}
