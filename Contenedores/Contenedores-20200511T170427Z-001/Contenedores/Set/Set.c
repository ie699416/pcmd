/*
 * Set.c
 *
 *  Created on: 20/04/2016
 *      Author: hpiza
 */
#include "Set.h"
#include <stdlib.h>
#include <stdio.h>

struct strNode {
	Type data;
	struct strNode *leftChild, *rightChild, *parent;
};
typedef struct strNode *Node;

struct strSet {
	Node root;
	int size;
	CompareFunc compare;
};

Set set_create(CompareFunc cf) {
	Set set = (Set) malloc(sizeof(struct strSet));
	set->root    = NULL;
	set->compare = cf;
	set->size    = 0;
	return set;
}

Node createNode(Type t) {
	Node n = (Node) malloc(sizeof(struct strNode));
	n->data = t;
	n->leftChild  = NULL;
	n->rightChild = NULL;
	n->parent     = NULL;
	return n;
}

Bool set_add(Set s, Type t) {
	if(s->root == NULL) {
		s->root = createNode(t);
		s->size = 1;
		return TRUE;
	}
	Node node  = s->root;
	Bool added = FALSE;
	Bool found = FALSE;
	while(!added && !found) {
		int result = s->compare(t, node->data);
		if(result == 0) { 		// Dato encontrado
			found = TRUE;
		}
		else if(result < 0) {  	// Ir al lado izquierdo
			if(node->leftChild == NULL) {
				node->leftChild = createNode(t);
				node->leftChild->parent = node;
				added = TRUE;
			} else {
				node = node->leftChild;
			}
		} else {  				// Ir al lado derecho
			if(node->rightChild == NULL) {
				node->rightChild = createNode(t);
				node->rightChild->parent = node;
				added = TRUE;
			} else {
				node = node->rightChild;
			}
		}
	}
	if(added) s->size ++;
	return added;
}

int set_size(Set s) {
	return s->size;
}

Bool set_contains(Set s, Type t) {
	if(s->root==NULL){
			return FALSE;
		}
	Bool found= FALSE;
	Node aux = s->root;
	while(!found && aux!=NULL){
		int r=s->compare(t,aux->data);
		if(r==0){
			found=TRUE;
		}
		else if(r<0){
			aux = aux->leftChild;
		}
		else{
			aux = aux->rightChild;
		}
	}
	return found;
}

void print(Node node, void (*printFunc)(Type t, int spaces), int spaces) {
	if(node == NULL) return;
	printFunc(node->data, spaces);
	print(node->leftChild, printFunc, spaces + 1);
	print(node->rightChild, printFunc, spaces + 1);
}

void set_print(Set s, void (*printFunc)(Type t, int spaces)) {
	if(s->root == NULL) return;
	Type d = s->root->data;
	printFunc(d, 0);
	print(s->root->leftChild, printFunc, 1);
	print(s->root->rightChild, printFunc, 1);
}

void destroy(Node n) {
	if(n == NULL) return;
	destroy(n->leftChild);
	destroy(n->rightChild);
	printf("%6d\n", *((int*) n->data));
	free(n);
}

void set_destroy(Set s) {
	destroy(s->root);
	free(s);
}

typedef enum { LEFT, RIGHT, ROOT } ChildType;

void addLink(Node parent, Node child, ChildType type) {
	if(type == LEFT) parent->leftChild = child;
	else parent->rightChild = child;
	if(child != NULL) child->parent = parent;
}

void removeBranch(Set s, Node n, ChildType type) {
	//	Caso 1: no tiene hijos
	if(n->leftChild == NULL && n->rightChild == NULL) {
		if(type == ROOT) s->root = NULL;
		else addLink(n->parent, NULL, type);
//		if(type == LEFT) n->parent->leftChild = NULL;
//		else if(type == RIGHT) n->parent-> rightChild = NULL;
//		else s->root = NULL;
		free(n);
	}

	//	Caso 2: tiene hijo izquierdo
	else if(n->leftChild && n->rightChild == NULL){
		if(type == ROOT)
			s->root = n->leftChild;
		else
			addLink( n->parent, n->leftChild, type);
		free(n);
	}
	//	Caso 3: tiene hijo derecho
	else if(n->leftChild == NULL && n->rightChild){
			if(type == ROOT)
				s->root = n->rightChild;
			else
				addLink( n->parent, n->rightChild, type);
		free(n);
	}

	//	Caso 4: tiene dos hijos
	else {
		printf("Caso 4\n");
		fflush(stdout);
		Node bigger = n->leftChild;
		while(bigger->rightChild != NULL){
			bigger = bigger->rightChild;
		}
		n->data = bigger->data;
		if(bigger->parent == n || bigger->parent == s->root){
			addLink(bigger->parent, bigger->leftChild, LEFT);
		}
		else{
			addLink(bigger->parent, bigger->leftChild, RIGHT);
		}
		free(bigger);
	}

}

Bool set_remove(Set s, Type t) {
	if(s->root == NULL) return FALSE;
	Bool found = FALSE;
	Node aux = s->root;
	ChildType childType = ROOT;
	while(!found && aux != NULL) {
		int r = s->compare(t, aux->data);
		if(r == 0) {
			removeBranch(s, aux, childType);
			found = TRUE;
		}
		else if(r < 0) {
			aux = aux->leftChild;
			childType = LEFT;
		}
		else {
			aux = aux->rightChild;
			childType = RIGHT;
		}
	}
	if(found) s->size --;
	return found;
}
