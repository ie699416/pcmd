/*
 * List.h
 *
 *  Created on: 13/04/2016
 *      Author: hpiza
 */

#ifndef LIST_H_
#define LIST_H_

typedef void* Type;
typedef struct strList* List;

List list_create();
void list_destroy(List);
void list_add(List, Type);
int list_size(List);

Type list_get(List, int index);
void list_set(List, Type, int index);
Type list_remove(List, int index);
void list_insert(List, int index);

#endif /* LIST_H_ */
