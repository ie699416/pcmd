/*
 * List.c

 *
 *  Created on: 13/04/2016
 *      Author: hpiza
 */
#include "List.h"
#include <stdlib.h>

struct strNode {
	Type data;
	struct strNode *prior, *next;
};

typedef struct strNode* Node;

struct strList {
	Node first, last;
	int size;
};

List list_create() {
	List l = (List) malloc(sizeof(struct strList));
	l->first = l->last = NULL;
	l->size = 0;
	return l;
}

void list_destroy(List l) {
	Node aux = l->first;
	Node aux2;
	while(aux != NULL) {
		aux2 = aux->next;
		free(aux);
		aux  = aux2;
	}
	free(l);
}

void list_add(List l, Type t) {
	Node n = (Node) malloc (sizeof(struct strNode));
	n->data = t;
	n->next = NULL;
	if (l->size == 0){
		n->prior = NULL;
		l->first = n;
	}
	else {
		n->prior = l->last;
		l->last->next = n;
	}
	l->last = n;
	l->size++;
}

int list_size(List l) {
	return l->size;
}

Type list_get(List l, int index) {
	if(index < 0 || index >= l->size) return NULL;
	Node aux = l->first;
	int i;
	for(i = 0; i < index; i ++) aux=aux->next;
	return aux->data;
}

