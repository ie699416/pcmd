/*
 * List.h
 *
 *  Created on: 13/04/2016
 *      Author: hpiza
 */

#ifndef LIST_H_
#define LIST_H_

typedef int Type;
typedef struct strList* List;
typedef enum {FALSE, TRUE }Bool;
typedef struct strNode* Iterator;

List list_create();
void list_destroy(List);
void list_add(List, Type);
int list_size(List);

Type list_get(List, int index);
void list_set(List, Type, int index);
Type list_remove(List, int index);
void list_insert(List, int index);

Iterator list_begin(List);
Iterator list_end(List);
Bool list_hasNext(Iterator);
Bool list_hasPrior(Iterator);
Iterator list_next(Iterator);
Iterator list_prior(Iterator);
Type list_data(Iterator);

#endif /* LIST_H_ */
