/*
 * Main.c
 *
 *  Created on: 13/04/2016
 *      Author: hpiza
 */
#include <stdio.h>
#include <stdlib.h>
#include <List.h>

typedef struct {
	int soyFeliz;
	char noTanto;
} Dumb;

int main() {
	List l1 = list_create();
	list_add(l1, "hola");
	list_add(l1, "mundo cruel");
	list_add(l1, "adios");

	float f = 5.6;
	list_add(l1, &f);

	long l = 1530214;
	list_add(l1, &l);

	Dumb d = {100, 'F'};
	list_add(l1, &d);

	Dumb *pd = (Dumb*) malloc(sizeof(Dumb));
	pd->soyFeliz = 85;
	pd->noTanto = '*';
	list_add(l1, pd);

	int array[] = {65, 2, 3};
	list_add(l1, array);

	printf("%d\n", list_size(l1));

	int i;
	for(i = 0; i < list_size(l1); i ++) {
//		char* c = (char*) list_get(l1, i);
//		puts(c);
		int *d = (int*) list_get(l1, i);
		printf("%d\n", *d);
	}

	list_destroy(l1);
	return 0;
}
