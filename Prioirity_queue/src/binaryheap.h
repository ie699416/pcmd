/*
 * binaryheap.h
 *
 *  Created on: May 12, 2020
 *      Author: C20256
 */

#ifndef BINARYHEAP_H_
#define BINARYHEAP_H_

#include <stdlib.h>
#include "type.h"

typedef int (*Comparator)(void *, void *);
typedef void (*Swap)(void *, void *);
typedef void * (*Clone)(void *);

typedef struct{
Comparator cmpFunction;
Swap swap;
Clone clone;
}HeapOperations;

typedef void *Type;
typedef struct strHeap *Heap;

/*Crea un Binary Heap nuevo sin elementos.*/
Heap heap_create(HeapOperations, int);

/*Agrega un nuevo elemento al Heap*/
void heap_insert(Heap, Type);

/*Elimina el elemento m�ximo almacenado en el Heap*/
Type heap_deleteMax(Heap);

/*B�sca el elemento m�ximo almacenado en el Heap*/
Type heap_findMax(Heap);

/*Retorna true si el Heap no tiene elementos, en otro caso retorna false.*/
Boolean heap_isEmpty(Heap);

/*Retorna el n�mero de elementos en el Heap*/
unsigned int heap_size(Heap);

/*Construye un Max-Heap a partir de una arreglo de elementos Type. */
Heap heap_build(Type *, int);





#endif /* BINARYHEAP_H_ */
