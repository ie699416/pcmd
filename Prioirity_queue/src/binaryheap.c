/*
 * binaryheap.c
 *
 *  Created on: May 12, 2020
 *      Author: C20256
 */


#include "binaryheap.h"

struct strHeap {
	unsigned int size;
	unsigned int capacity;
	Type *Array;
	HeapOperations hOperations;
};

/*Crea un Binary Heap nuevo sin elementos.*/
Heap heap_create(HeapOperations ops, int capacity) {
	Heap newHeap = (Heap) malloc(sizeof(struct strHeap));
	newHeap->size = 0;
	newHeap->capacity = capacity;
	newHeap->Array = (Type) malloc(capacity * sizeof(Type));
	newHeap->hOperations = ops;
	return newHeap;
}

///*Agrega un nuevo elemento al Heap*/
//void heap_insert(Heap who, Type data) {
//	who->Array[who->size] = who->hOperations->clone(data);
//	proclateUp(who->Array, who->size, who->hOperations);
//	who->size++;
//}
//
//void proclateUp(Type *array, unsigned int position, HeapOperations hOps) {
//	unsigned int parent = (position - 1) / 2;
//	if (position != 0) {
//		if (hOps->cmpFunction(array[parent], array[position]) > 0) {
//			hOps->swap(&array[parent], &array[position]);
//			proclateUp(array, parent, hOps);
//		}
//	}
//}
//
///*Elimina el elemento m�ximo almacenado en el Heap*/
//Type heap_deleteMax(Heap who) {
//	Type value = who->Array[0];
//	who->Array[0] = who->Array[who->size - 1];
//	who->size--;
//	proclateDown(who->Array, 0, who->size, who->hOperations);
//	return value;
//}
//
//void proclateDown(Type *array, unsigned int position, unsigned int size,
//		HeapOperations hOps) {
//	int lchild = 2 * position + 1;
//	int rchild = lchild + 1;
//	int small = -1;
//	if (lchild < size)
//		small = lchild;
//	if ((rchild < size) && (hOps.cmpFunction(array[rchild], array[lchild]) > 0))
//		small = rchild;
//	if ((small != -1)
//			&& (hOps.cmpFunction(array[small], array[position]) > 0)) {
//		hOps.swap(&array[position], &array[small]);
//		proclateDown(array, small, size, hOps);
//	}
//}

///*B�sca el elemento m�ximo almacenado en el Heap*/
//Type heap_findMax(Heap) {
//
//
//}
//
///*Retorna true si el Heap no tiene elementos, en otro caso retorna false.*/
//Boolean heap_isEmpty(Heap) {
//
//}
//
///*Retorna el n�mero de elementos en el Heap*/
//unsigned int heap_size(Heap) {
//
//}
