/*
 * List.c
 *
 *  Created on: 26/04/2016
 *      Author: IE699416
 */

#include "List.h"
#include <stdlib.h>

struct strNode {
	Type data;
	struct strNode *prior, *next;
};

typedef struct strNode* Node;

struct strList {
	Node first, last, prefirst, postlast;
	int size;
};

List list_create() {
	List l = (List) malloc(sizeof(struct strList));
	l->prefirst = (Node) malloc(sizeof(struct strNode));
	l->postlast = (Node) malloc(sizeof(struct strNode));
	l->first = l->last = NULL;
	l->prefirst->next = NULL;
	l->postlast->prior = NULL;
	l->size = 0;
	return l;
}

void list_destroy(List l) {
	Node aux = l->first;
	Node aux2;
	while (aux != NULL) {
		aux2 = aux->next;
		free(aux);
		aux = aux2;
	}
	free(l->prefirst);
	free(l->postlast);
	free(l);
}

void list_add(List l, Type t) {
	Node n = (Node) malloc(sizeof(struct strNode));
	n->data = t;
	n->next = NULL;
	if (l->size == 0) {
		n->prior = NULL;
		l->first = n;
		l->prefirst->next = l->first;
	} else {
		n->prior = l->last;
		l->last->next = n;
	}
	l->last = n;
	l->postlast->prior = l->last;
	l->size++;
}

int list_size(List l) {
	return l->size;
}

Type list_get(List l, int index) {
	if (index < 0 || index >= l->size){
		return 0;
	}
	Node aux = l->first;
	int i;
	for (i = 0; i < index; i++)
		aux = aux->next;
	return aux->data;
}

Iterator list_begin(List l) {
	return l->prefirst;
}

Iterator list_end(List l) {
	return l->postlast;
}

Boolean list_hasNext(Iterator ite) {
	return ite->next != NULL;
}

Boolean list_hasPrior(Iterator ite) {
	return ite->prior != NULL;
}

Iterator list_next(Iterator ite) {
	return ite->next;
}

Iterator list_prior(Iterator ite) {
	return ite->prior;
}

Type list_data(Iterator ite) {
	return ite->data;
}


