/*
 * List.h
 *
 *  Created on: 26/04/2016
 *      Author: IE699416
 */

#ifndef LIST_H_
#define LIST_H_

#include "type.h"

typedef struct strList* List;

typedef struct strNode* Iterator;

List list_create();

void list_destroy(List);

void list_add(List, Type);

int list_size(List);

Type list_get(List, int index);

void list_set(List, Type, int index);

Type list_remove(List, int index);

Iterator list_begin(List);

Iterator list_end(List);

Boolean list_hasNext(Iterator);

Boolean list_hasPrior(Iterator);

Iterator list_next(Iterator);

Iterator list_prior(Iterator);

Type list_data(Iterator);

void list_set(List, Type, int index);

void list_insert(List, Type, int index);

//Type list_remove(List, int index);

void list_introduce(List, Iterator, Type);

Iterator list_delete(List, Iterator);

#endif /* LIST_H_ */
