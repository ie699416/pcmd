#include "navigator.h"

/*********************************************************************************************************************/
/* Navigator functions implementation*/

Navigator* navigator_createInstance() {
	Navigator *instance = (Navigator*) malloc(sizeof(Navigator));
	instance->historic_back = deque_create();
	instance->historic_forth = deque_create();
	instance->isForthInUse = FALSE;
	instance->first_filePathStr = NULL;
	instance->current_filePathStr = NULL;
	instance->last_filePathStr = NULL;
	return instance;
}

void navigator_destroyInstance(Navigator *instance) {
	deque_destroy(instance->historic_back);
	deque_destroy(instance->historic_forth);
	free(instance);

}

void navigator_openFile(Navigator *instance, char *filePath) {
	if (NULL == instance->current_filePathStr) {
		instance->current_filePathStr = strdup(filePath);
		instance->first_filePathStr = strdup(filePath);
		instance->last_filePathStr = strdup(filePath);
	} else {
		navigator_offerFile_back(instance);
		free(instance->current_filePathStr);
		instance->current_filePathStr = strdup(filePath);
		if (instance->isForthInUse) {
			/*Deletes and clear future*/
			deque_clear(instance->historic_forth);
			instance->isForthInUse = FALSE;
		}
	}
}

void navigator_offerFile_back(Navigator *instance) {
	deque_offer_front(instance->historic_back, instance->current_filePathStr);
}

void navigator_offerFile_forth(Navigator *instance) {
	instance->isForthInUse = TRUE;
	deque_offer_front(instance->historic_forth, instance->current_filePathStr);
}

/* Function: Polls the value from the back container
 * Prototype: void navigator_poll_back(Navigator);
 * Input: Navigator.
 * Description:
 * -	Calls the function navigator_offerFile_forth();
 * -	Calls the function deque_poll_front(navigator.historic_back);
 * -	Proceeds to cast the polled value to (Char*);
 * - 	The member navigator.current_filePathStr is updated with the casted value.
 * Returns: void
 */
char* navigator_poll_back(Navigator *instance) {
	/*Size criteria comparison is followed prior calling this function*/
	Type typeAux;
	char *stringAux;
	typeAux = deque_poll_front(instance->historic_back);
	stringAux = Type2String(typeAux);
	return stringAux;
}

/* Function: Polls the value from the back container
 * Prototype: void navigator_poll_back(Navigator);
 * Input: Navigator.
 * Description:
 * -	Calls the function navigator_offerFile_forth();
 * -	Calls the function deque_poll_front(navigator.historic_back);
 * -	Proceeds to cast the polled value to (Char*);
 * - 	The member navigator.current_filePathStr is updated with the casted value.
 * Returns: void
 */

char* navigator_poll_forth(Navigator *instance) {

	/*Size criteria comparison is followed prior calling this function*/
	Type typeAux;
	char *stringAux;
	typeAux = deque_poll_front(instance->historic_forth);
	if (ZERO == (deque_size(instance->historic_forth)))
		instance->isForthInUse = FALSE; /*Check and clear flag*/
	stringAux = strdup(Type2String(typeAux));
	return stringAux;
}

/* Function: Read from historic back
 * Prototype: Boolean navigator_read_back(Navigator);
 * Input: Navigator.
 * Description:
 * - 	If the back container is empty, returns FALSE.
 * -	Offers the current value to the historic forth container by calling navigator_offer_forth.
 * -	Calls navigator_poll_back and store it at current_filePathStr.
 * -	Returns TRUE.
 * Returns: Boolean
 */

Boolean navigator_read_back(Navigator *instance) {
	/*Size criteria comparison before polling*/
	if (deque_size(instance->historic_back)) {
		navigator_offerFile_forth(instance);
		free(instance->current_filePathStr);
		instance->current_filePathStr = strdup(navigator_poll_back(instance));

		return TRUE;
	} else {
		printf("\n\n\tContainer historic_back is empty. ");
		return FALSE;
	}
	return FALSE;
}

/* Function: Read from historic back
 * Prototype: Boolean navigator_read_forth(Navigator);
 * Input: Navigator.
 * Description:
 * - 	If the forth container is empty, returns FALSE.
 * -	Offers current_filePathStr to the historic back container by calling navigator_offer_back.
 * -	Polls the front value of the historic back container by calling navigator_poll_forth.
 * - 	The member navigator.current_filePathStr is updated with the polled container.
 * -	Checks if the forth container is empty, if so, changes navigator.isForthInUse to TRUE.
 * -	Returns TRUE.
 * Returns: Boolean
 */

Boolean navigator_read_forth(Navigator *instance) {
	/*Size criteria comparison before polling*/
	if (deque_size(instance->historic_forth)) {
		navigator_offerFile_back(instance);
		free(instance->current_filePathStr);
		instance->current_filePathStr = strdup(navigator_poll_forth(instance));
		return TRUE;
	} else {
		printf("\n\n\tContainer historic_forth is empty. ");
		return FALSE;
	}
	return FALSE;
}

/* Function: Read from historic back rear
 * Prototype: Boolean navigator_read_first(Navigator);
 * Input: Navigator.
 * Description:
 * -	Calls the function navigator_openFile() with the argument being the
 * 		string stored at the member navigator.first_filePath_str;   *
 * Returns: void
 */
void navigator_read_first(Navigator *instance) {

}

/* Function: Read from historic forth rear
 * Prototype: Boolean navigator_read_last(Navigator);
 * Input: Navigator.
 * Description:
 * -	Calls the function navigator_openFile() with the argument being the
 * 		string stored at the member navigator.last_filePath_str;  *
 *
 * Returns: void
 */

void navigator_read_last(Navigator *instance) {

}

/*********************************************************************************************************************/
/* Input/Output UI functions */

void getFilePath(char *filePath) {
	printf("\nEnter file path or name:");
	scanf("%s", filePath);
}

void setMainPath(char *folderPath) {
	printf("\nEnter texts folder path:");
	scanf("%s", folderPath);
	printf("\nFolder path: %s", folderPath);
}

int printMenu(char *buffer, unsigned int n) {
	unsigned int menu_option = ZERO;

	printf("\n\tFile options :");
	printf("\n\t\t- [1] Total words in file.");
	printf("\n\t\t- [2] Number of sentences.");
	printf("\n\t\t- [3] Show original text.");
	printf("\n\t\t- [4] Go back to file explorer.");
	printf("\n");

	scanf("%d", &menu_option);

	switch (menu_option) {
	case 0:
		return ERR;

		break;
	case 1:
		menu_1(buffer, n);
		break;

	case 2:
		menu_2(buffer, n);
		break;

	case 3:
		menu_3(buffer, n);
		break;

	case 4:
		menu_4(buffer, n);
		return TRUE;

	default:
		return ZERO;
	}

	return ZERO;
}

int print_menuContinue() {
	unsigned int wait = ZERO;
	printf("\n\tContinue?");
	printf("\n\t\t- [0] No (exit)");
	printf("\n\t\t- [1] Yes (Get back to main menu)");

	while (1) {
		scanf("%d", &wait);
		switch (wait) {
		case 0:
			return ERR;
		case 1:
			return ZERO;
		default:
			continue;
		}
	}
	return TRUE;
}

void menu_1(char *buffer, unsigned int n) {
	unsigned int words = countWords(buffer, n);
	printf("\n\n\tTotal number of words in file: %d\n", words);
}

void menu_2(char *buffer, unsigned int n) {
	unsigned int sentences = countSentences(buffer, n);
	printf("\n\n\tTotal number of sentences in file: %d\n", sentences);
}

void menu_3(char *buffer, unsigned int n) {
	printBufferText(buffer, n);
}

void menu_4(char *buffer, unsigned int n) {
	printf("\n\n\tClosing file.\n");
}

