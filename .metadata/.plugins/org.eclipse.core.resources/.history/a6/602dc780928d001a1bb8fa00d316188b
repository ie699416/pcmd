#ifndef NAVIGATOR_H_
#define NAVIGATOR_H_

#include "type.h"
#include "textprocessor.h"
#include "deque.h"

/*********************************************************************************************************************/
/*ADT definitions*/

typedef struct strNavigator{
	Deque historic_back;				// Queue container on which all previous files references are kept.
	Deque historic_forth;				// Queue container that serves as a back up to return to the future.
	Boolean isForthInUse;				// Flag that indicates if the historic_forth contains any references
	char * first_filePathStr;			// Will always remember the first file that was loaded.
	char * current_filePathStr;			// When loading a new file, the current reference is kept here.
	char * last_filePathStr;			// Will always remember the last new file that was loaded.
} Navigator;
/*********************************************************************************************************************/
/*Navigator Function definitions*/

/* Function: Create Navigator Instance
 * Prototype: Navigator navigator_createInstance(void);
 * Input: void
 * Description:
 * -	The navigator instance is created with MALLOC. *
 * -	The members are initialized as:
 *
 * 		navigator.historic_back: a deque container is created and initialized with the function deque_create();
 *
 * 		navigator.historic_forth: a deque container is created and initialized with the function deque_create();
 *
 * 		navigator.isForthInUse: Set to FALSE.
 *
 * 		navigator.first_filePathStr: Points to NULL.
 *
 * 		navigator.current_filePathStr: Points to NULL.
 *
 * 		navigator.last_filePathStr: Points to NULL.
 *
 * 	Returns: navigator instance address.
 * Returns: navigator
 */

Navigator * navigator_createInstance(void);

/* Function: Destroy Navigator Instance
 * Prototype: void navigator_destroyInstance(Navigator);
 * Input: Navigator
 * Description:
 * -	The member navigator.historic_forth is freed through the function deque_destroy().
 * -	The member navigator.historic_back is freed through the function deque_destroy().
 * -	At last, the navigator instance is freed .
 * Returns: void function.
 */

void navigator_destroyInstance(Navigator *);

/* Function: Open new file within navigator
 * Prototype: void navigator_openFile(Navigator, char *, char *);
 * Input: Navigator, char *, char *.
 * Description:
 * -	If the member navigator.current_filePath_str == NULL:
 * 		>>	Meaning this is the first file opened, the value of navigator.firstFilePath_str is updated.
 * 		>>  The value of navigator.lastFilePath_str is updated.
 * 		>>  The value of navigator.currentFilePath_str is updated.
 *
 * -	If the member navigator.current_filePath_str != NULL:
 * 		>>  Checks if the file to open is already referenced by current_filePathStr.
 * 		>>	The function will use navigator_offerFile_back() to offer the current value to the container.
 * 		>>  The value of navigator.lastFilePath_str is updated.
 *
 * - 	The navigator instance will load a new file by adding the new reference to the current position.
 * -	Later, the filereader will use the file PATH stored at current to open the file.
 * -	Compares the value of the flag navigator.isForthInUse, if TRUE, clears the forth container.
 * Returns: void
 */

void navigator_openFile(Navigator *, char *);

/* Function: Add a new reference to the navigator historic container back.
 * Prototype: void navigator_offerFile_back(Navigator);
 * Input: Navigator.
 * Description:
 * -	Offer a new element to the member navigator.historic_back.
 * -	The member to be offered is located within the member navigator.current_filePathStr;
 * Note: The value of navigator.current_filePathStr remains with the same value. *
 * Returns: void
 */

void navigator_offerFile_back(Navigator * );

/* Function: Add a new reference to the navigator historic container forth.
 * Prototype: Boolean navigator_offerFile_forth(Navigator);
 * Input: Navigator.
 * Description:
 * -	Offer a new element to the member navigator.historic_forth.
 * -	The member to be offered is located within the member navigator.current_filePathStr;
 *  -	Always updates the value of navigator.isForthInUse to TRUE.
 * Note: The value of navigator.current_filePathStr remains with the same value.
 * Returns: void
 */

void navigator_offerFile_forth(Navigator * );

/* Function: Polls the value from the back container
 * Prototype: Boolean navigator_poll_back(Navigator);
 * Input: Navigator.
 * Description:
 * -	Calls the function deque_poll_front(navigator.historic_back);
 * -	Proceeds to cast the polled value to (Char*) by calling Type2String();
 * -	Returns the string.
 * Returns: Boolean
 */

char * navigator_poll_back(Navigator * );

/* Function: Polls the value from the back container
 * Prototype: char * navigator_poll_back(Navigator);
 * Input: Navigator.
 * Description:
 * -	Calls the function deque_poll_front(navigator.historic_forth;
 * -	Checks if the container still holds any element,
 * 		>> if not, changes the value of flag is ForthInUse to FALSE.
 * -	Proceeds to cast the polled value to (Char*) by calling Type2String();
 * -	Returns the string.
 * Returns: char *
 */

char * navigator_poll_forth(Navigator * );

/* Function: Read from historic back
 * Prototype: Boolean navigator_read_back(Navigator);
 * Input: Navigator.
 * Description:
 * - 	If the back container is empty, returns FALSE.
 * -	Offers the current value to the historic forth container by calling navigator_offer_forth.
 * -	Polls the front value of the historic back container by calling navigator_poll_back.
 * -	Returns TRUE.
 * Returns: Boolean
 */


Boolean navigator_read_back(Navigator *);

/* Function: Read from historic back
 * Prototype: Boolean navigator_read_forth(Navigator);
 * Input: Navigator.
 * Description:
 * - 	If the forth container is empty, returns FALSE.
 * -	Offers current_filePathStr to the historic back container by calling navigator_offer_back.
 * -	Polls the front value of the historic back container by calling navigator_poll_forth.
 * - 	The member navigator.current_filePathStr is updated with the polled container.
 * -	Checks if the forth container is empty, if so, changes navigator.isForthInUse to TRUE.
 * -	Returns TRUE.
 * Returns: Boolean
 */

Boolean navigator_read_forth(Navigator *);

/* Function: Read from historic back rear
 * Prototype: Boolean navigator_read_first(Navigator);
 * Input: Navigator.
 * Description:
 * -	Calls the function navigator_openFile() with the argument being the
 * 		string stored at the member navigator.first_filePath_str;
 * Returns: void
 */
void navigator_read_first(Navigator *);

/* Function: Read from historic forth rear
 * Prototype: Boolean navigator_read_last(Navigator);
 * Input: Navigator.
 * Description:
 * -	Calls the function navigator_openFile() with the argument being the
 * 		string stored at the member navigator.last_filePath_str;
 *
 * Returns: void
 */

void navigator_read_last(Navigator *);


/*********************************************************************************************************************/
/*Input / output UI Function definitions*/

/* Function: Prints continue screen
 * Prototype: void print_menuContinue(void );
 * Input: void
 * Description: Waits for an input to exit or continue.
 * Returns: int
 */

int print_menuContinue(void );

/* Function: Capture text file path or name:
 * Prototype: void getPath(char *, char * );
 * Input: char *, char *
 * Description: This function makes use of the scanf() function to ask the user for a path.
 * Returns: void
 */

void getFilePath(char * );

/* Function: Capture folder PATH:
 * Prototype: void getPath(char * );
 * Input: char *
 * Description: This function makes use of the scanf() function to ask the user for a path.
 * Returns: void
 */

void setMainPath(char * );

/* Function: Print menu
 * Prototype: void printMenu(void);
 * Input: MMU *instance
 * Description: Prints the 8 options and calls scanf() to wait for the user input.
 * Returns: integer
 */

int printMenu(char * buffer, unsigned int n);

/* Function: [1] Number of sentences separated by dots.
 * Prototype:void menu_2 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */

void menu_1 (char * , unsigned int );
/* Function: [2] Number of sentences separated by dots.
 * Prototype:void menu_2 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */

void menu_2 (char * , unsigned int );

/* Function: [3] Sort text sentences A-Z
 * Prototype:void menu_3 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -	.
 * Returns: void
 */

void menu_3 (char * , unsigned int );

/* Function: [4] Sort text sentences Z-A.
 * Prototype: void menu_4 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */

void menu_4 (char * , unsigned int );

/* Function: [5] Sort text sentences larger to shorter.
 * Prototype: void menu_5 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */



#endif /* NAVIGATOR_H_ */
