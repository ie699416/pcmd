/*
 * List.c
 *
 *  Created on: 26/04/2016
 *      Author: IE699416
 */

#include "List.h"
#include <stdlib.h>

struct strNode {
	Type data;
	struct strNode *prior, *next;
};

typedef struct strNode *Node;

struct strList {
	Node first, last, prefirst, postlast;
	int size;
};

List list_create() {
	List l = (List) malloc(sizeof(struct strList));
	l->prefirst = (Node) malloc(sizeof(struct strNode));
	l->postlast = (Node) malloc(sizeof(struct strNode));
	l->first = l->last = NULL;
	l->prefirst->next = NULL;
	l->postlast->prior = NULL;
	l->size = 0;
	return l;
}

void list_destroy(List l) {
	Node aux = l->first;
	Node aux2;
	while (aux != NULL) {
		aux2 = aux->next;
		free(aux);
		aux = aux2;
	}
	free(l->prefirst);
	free(l->postlast);
	free(l);
}

void list_add(List l, Type t) {
	Node n = (Node) malloc(sizeof(struct strNode));
	n->data = t;
	n->next = NULL;
	if (l->size == 0) {
		n->prior = NULL;
		l->first = n;
		l->prefirst->next = l->first;
	} else {
		n->prior = l->last;
		l->last->next = n;
	}
	l->last = n;
	l->postlast->prior = l->last;
	l->size++;
}

int list_size(List l) {
	return l->size;
}

Type list_get(List l, int index) {
	if (index < 0 || index >= l->size) {
		return 0;
	}
	Node aux = l->first;
	int i;
	for (i = 0; i < index; i++)
		aux = aux->next;
	return aux->data;
}



Type list_remove(List l, int p) {
	Node current = NULL;
	Type tmp;
	int i = 0;
	int s = l->size;
	if (l != NULL) {
		//Buscar el nodo a remover
		if ((p >= 0) && (p < s)) {
			current = l->first;
			while (i < p) {
				current = current->next;
				i++;
			}
		}
		if ((p == 0) && (s == 1)) {
			tmp = current->data;
			l->first = NULL;
			l->last = NULL;
			l->size = 0;
			free(current);
			return tmp;
		} else {
			if (p == 0) //Se va a eliminar el nodo inicial
					{
				tmp = current->data;
				l->first = current->next;
				l->first->prior = NULL;
				l->size--;
				free(current);
				return tmp;
			} else {
				if (p == (s - 1)) //Se va a eliminar el nodo final
						{
					tmp = current->data;
					l->last = current->prior;
					l->last->next = NULL;
					l->size--;
					free(current);
					return tmp;
				} else { //El elemento a remover esta en medio
					tmp = current->data;
					current->prior->next = current->next;
					current->next->prior = current->prior;
					free(current);
					return tmp;
				}
			}
		}

	}
	return NULL;
}


Iterator list_begin(List l) {
	return l->prefirst;
}

Iterator list_end(List l) {
	return l->postlast;
}

Boolean list_hasNext(Iterator ite) {
	return ite->next != NULL;
}

Boolean list_hasPrior(Iterator ite) {
	return ite->prior != NULL;
}

Iterator list_next(Iterator ite) {
	return ite->next;
}

Iterator list_prior(Iterator ite) {
	return ite->prior;
}

void list_move_itt2prior(Iterator * ite) {
	*ite = list_prior(*ite);
}

void list_move_itt2next(Iterator * ite) {
	*ite = list_next(*ite);
}

Type list_data(Iterator ite) {
	return ite->data;
}

