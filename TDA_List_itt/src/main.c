/*
 * main.c
 *
 *  Created on: May 11, 2020
 *      Author: C20256
 */

#include "list.h"
#include "type.h"

int main(void) {

	List list = list_create();
	Iterator ite_begin = list_begin(list);
	Iterator ite_end = list_end(list);
	char char_aux;

	list_add(list, Char('h'));
	list_add(list, Char('o'));
	list_add(list, Char('l'));
	list_add(list, Char('a'));
	list_add(list, Char(' '));
	list_add(list, Char('m'));
	list_add(list, Char('u'));
	list_add(list, Char('n'));
	list_add(list, Char('d'));
	list_add(list, Char('o'));

	for (int i = 0; i <= list_size(list); i++) {
		if (list_hasNext(ite_begin)) {
			list_move_itt2next(&ite_begin);
			char_aux = Type2Char(list_data(ite_begin));
			printf("%c", char_aux);
		}
	}

	printf("\n");

	for (int i = list_size(list); i >= 0  ; i--) {
		if (list_hasPrior(ite_end)) {
			list_move_itt2prior(&ite_end);
			char_aux = Type2Char(list_data(ite_end));
			printf("%c", char_aux);
		}
	}

	return 0;
}

