/*
 ============================================================================
 Name        : main.c
 Author      : ie699416
 Version     : 1.1
 Description : Dynamic memory function
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

int* newIntArray(unsigned int);
char** newCharMatrix(char, char);
void printCharMatrix(char, char, char**);

void printArray(int, int*);

int main(void) {
	char **a = newCharMatrix(4, 4);
	printCharMatrix(4, 4, a);

	return 0;
}

void printArray(int n, int *array) {
	for (int i = 0; i < n; i++) {
		printf(" %d", array[i]);
	}
}

int* newIntArray(unsigned int numel) {
	return (int*) calloc(numel, sizeof(unsigned int));
}

char** newCharMatrix(char rows, char cols) {
	char **matPtr = (char**) malloc(rows * sizeof(char*));
	for (int i = 0; i < rows; i++) {
		matPtr[i] = (char*) malloc(cols * sizeof(char));
		for (int j = 0; j < cols; j++)
			matPtr[i][j] = 'a';
	}
	return matPtr;
}

void printCharMatrix(char rows, char cols, char **matPtr) {
	for (int i = 0; i < rows; i++) {
		printf("[");
		for (int j = 0; j < cols; j++)
			printf(" %c", matPtr[i][j]);
		printf(" ]\n");
	}
}
