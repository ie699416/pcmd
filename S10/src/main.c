/*
 ============================================================================
 Name        : main.c
 Author      : ie699416
 Version     : 1.1
 Description : Main program file.
 ============================================================================
 */

#include "alumno.h"



int main(void) {
	/*Init*/
	boolean b;
	printf("Size of boolean: %d\n",sizeof(boolean));
	printf("Size of b: %d\n",sizeof(b));
	Subject PcMD = newSubject("PcMD");
	Student *ie699416 = newStudent(699416,"Oscar Maisterra",8.02);
	Student *is707070 = newStudent(707070,"Ana Aguilar",10.00);
	Student **students[] = {&ie699416, &is707070};

	enrollStudent(*students[0], &PcMD);
	enrollStudent(*students[1], &PcMD);
	updateAverage(*students[0], 8.5);
	printSubject(&PcMD);


	eraseStudent(*students[0]);
	printSubject(&PcMD);

	return 0;
}

