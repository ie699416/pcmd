/*
 ============================================================================
 Name        : alumno.h
 Author      : ie699416
 Version     : 1.1
 Description : Basic Container library header file.
 ============================================================================
 */

#ifndef ALUMNO_H_
#define ALUMNO_H_

#define FALSE 	0
#define TRUE	1

typedef enum{false, true} boolean;

typedef struct str_student {
	int exp;					// student ID
	char * name;				// Dynamic array of student name
	float avg;					// Grades mean or average
} Student;

typedef struct str_subject {
	Student ** students_ptr;	// Dynamic array of students data
	int n;						// Number of students
	char * name;				// Subject name
} Subject;


Student * newStudent(unsigned int, const char*, float);

Subject newSubject(const char *);

void printStudent(Student *);

void printSubject(Subject *);

void updateStudentName(Student *, const char *);

void updateAverage(Student *, float);

boolean eraseStudent(Student *);

boolean enrollStudent(Student *, Subject *);

boolean isStudentEnrolled(unsigned int, Subject *);



void updateSubjectName(Subject *, const char *);

boolean unEnrollStudent(Student *, Subject *);

boolean eraseSubject(Subject *);

void printStudents(Subject *);

#endif /* ALUMNO_H_ */
