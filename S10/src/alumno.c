/*
 ============================================================================
 Name        : alumno.c
 Author      : ie699416
 Version     : 1.1
 Description : Basic Container library implementation file.
 ============================================================================
 */

#include "alumno.h"
#include<string.h>
#include<stdlib.h>
#include<stdio.h>

/*Escribe una funci�n llamada nuevoAlumno que reciba como par�metros:
 * a) expediente
 * b) nombre
 * c) promedio.
 *
 * La funci�n deber� reservar memoria din�mica para almacenar los datos
 * (utilice un apuntador tipo Alumno).
 *
 * Al finalizar deber� retornar la direcci�n de memoria en donde se encuentra
 * almacenada la informaci�n.
 */

Student* newStudent(unsigned int exp, const char *name, float avg) {
	Student *new = (Student*) malloc(sizeof(Student));
	new->avg = avg;
	new->exp = exp;
	new->name = (char*) strdup(name);
	return new;
}

/*
 * Implemente una funci�n llamada crearAsignatura que reciba como par�metro
 * el nombre de la materia. La funci�n deber� reservar memoria din�mica e
 * inicializar los valores de sus elementos. El n�mero de inscritos inicialmente
 * debe ser 0 y el arreglo de tipo Alumno inicialmente debe tener longitud 0
 * (se utiliza un apuntador haciendo referencia a NULL).
 */

Subject newSubject(const char *name) {
	Subject *new = (Subject*) malloc(sizeof(Subject));
	new->n = 0;
	new->students_ptr = (Student**) NULL;
	new->name = (char*) strdup(name);
	return *new;
}

/* Escribe una funci�n llamada imprimirAlumno que reciba como par�metro
 * la direcci�n de memoria en donde se encuentra la informaci�n del alumno
 * que quiere imprimir.
 */

void printStudent(Student *student) {
	printf("\n\n\tStudent name        : %s", student->name);
	printf("\n\tStudent ID          : %d", student->exp);
	printf("\n\tStudent score       : %.2f", student->avg);
}

/* Escribe una funci�n llamada imprimirAlumno que reciba como par�metro
 * la direcci�n de memoria en donde se encuentra la informaci�n del alumno
 * que quiere imprimir.
 */

void printSubject(Subject *subject) {
	printf("\n\n\tSubject name        : %s", subject->name);
	printf("\n\tEnrolled students   : %d", subject->n);
	printStudents(subject);
}

/*
 * Escribe una funci�n llamada actualizarNombre que reciba como par�metro:
 * direcci�n de memoria donde se encuentra almacenada la informaci�n del alumno,
 * el nuevo nombre del alumno. La funci�n deber� actualizar el nombre.
 * (cuidado con la memoria din�mica).
 */

void updateStudentName(Student *student, const char *newName) {
	free(student->name);
	student->name = (char*) strdup(newName);
}

/*
 * Escribe una funci�n llamada actualizarPromedio que reciba como par�metro:
 * direcci�n de memoria donde se encuentra almacenada la informaci�n del alumno,
 * y el nuevo promedio del alumno. La funci�n deber� actualizar el promedio.
 */

void updateAverage(Student *student, float newAvg) {
	student->avg = newAvg;
}

/*
 * Implementemos una funci�n llamada InscribirAlumno que reciba como par�metros:
 * la direcci�n de memoria donde se encuentra la informaci�n del alumno que vamos
 * a registrar, y la direcci�n de memoria en donde se encuentra la informaci�n
 * de la asignatura (se supone fue creada anteriormente).
 * La funci�n internamente debe realizar lo siguiente:
 * - 	Verificar que el n�mero de expediente no este repetido
 * 		(puede utilizar la funci�n del ejercicio 11)
 * -	Reservar la memoria requerida
 * -	Actualizar el n�mero de alumnos inscritos
 * -	Notificar si la operaci�n fue exitosa o hubo un error.
 */

boolean enrollStudent(Student *student, Subject *subject) {
	boolean enrolled = FALSE; // Flag variable used in second stage of the the function

	/* As the first stage of the function, a loop runs a comparative linear algorithm
	 * to identify if the student is already enrolled.	 *
	 * 	- If the number of students (n) is zero, this comparative gets skipped.
	 */
	if (subject->n == 0)
		enrolled = FALSE;
	else
		enrolled = isStudentEnrolled(student->exp, subject);

	/* If the "enrolled" flag remains as FALSE:
	 * 	- The dynamic array of students data has to reserve memory for one additional element
	 * 	- The student data is added into the subject students array at the last position.
	 */

	if (!enrolled) {
		int newN = (subject->n) + 1;		// Update students counter
		subject->n = newN;
		Student **studentPtr = &student;

		/*Reserves new memory space*/
		Student **newMem = (Student**) malloc(newN * sizeof(Student*));

		/*copy n Student*'s from subject->students_ptr to newMem[0]...newMem[newN-2]*/
		if (newN - 1 != 0)
			memcpy(newMem, subject->students_ptr,
					(newN - 1) * sizeof(Student*));

		/*copy 1 Student* from student to newMem[newN-1]...newMem[newN]*/
		memcpy(newMem + (newN - 1), studentPtr, 1 * sizeof(Student*));

		/*Free old memory*/
		free(subject->students_ptr);

		/*Assign newMem into subject->students_ptr*/
		subject->students_ptr = newMem;

		/*Console validation*/
		printf("\n\n\tStudent '%s' enrolled successfully into subject %s!",
				subject->students_ptr[newN - 1]->name, subject->name);
		return TRUE;
	} else {
		printf("\nUnable to enroll %s, into subject %s.", student->name,
				subject->name);
		printf("\nStudent is already enrolled.");
		return FALSE;

	}
}

/*
 * Implementemos una llamada existeAlumno que reciba como par�metros el n�mero
 * de expediente de un alumno, y la direcci�n de memoria en donde se encuentra
 * la informaci�n de la asignatura. La funci�n debe recorrer el arreglo que contiene
 * a los alumnos inscritos;
 *
 * si el expediente ya existe.
 * 			La funci�n debe retornar True;
 * si el expediente no existe,
 * 			la funci�n debe retornar False.
 */

boolean isStudentEnrolled(unsigned int exp, Subject *subject) {
	for (int i = 0; i < subject->n; i++) {
		if ((subject->students_ptr[i]->exp) == exp)
			return TRUE;
	}
	return FALSE;
}

/*
 * Escribe una funci�n llamada eliminarAlumno que reciba como par�metro la
 * direcci�n de memoria en donde se encuentra la informaci�n del alumno que
 * quiere eliminar. La funci�n deber� liberar la memoria utilizada y retornar TRUE
 * (para indicar que la memoria fue exitosamente liberada).
 */

boolean eraseStudent(Student *student) {
	printf("\n\n\tStudent '%s' removed successfully!",student->name);
	free(student->name);
	free(student);
	return TRUE;
}

/*
 * La funci�n actualiza el nombre de la asignatura.
 */

void updateSubjectName(Subject *subject, const char *newName) {
	free(subject->name);
	subject->name = (char*) strdup(newName);
}

/*
 * Se describe la funcion inversa a la inscripcion del alumno a la asignatura.
 * Para dar de baja a un alumno se tiene que eliminar el elemento del contenedor.
 *
 * - Primero se valida si la asignatura tiene al menos 1 alumno,
 * 		... En caso de que n = 0, regresa FALSE.
 * - Ahora se procede a validar si el expediente esta registrado en el contenedor asignatura.
 * 		... De no pertenecer, regresa FALSE.
 * - Se identifica la posicion del alumno a dar de baja a tra ves de un ciclo for.
 *
 * - Para liberar al elemento del arreglo dinamico del apuntador de estudiantes inscritos:
 *   1) Se reserva un bloque de memoria para el nuevo arreglo de student*
 *   2) Se copia elemento por elemento hasta encontrar el que se quiere omitir.
 *   3) Se copia parte alta del arreglo restante.
 *   4) El nuevo arreglo dinamico es apuntado por el elemento students_ptr **.
 *   5) Se libera la memoria del elemento anterior.
 *  *
 * Al ejectuar esta funcion la informacion del alumno no es liberada en memoria.
 *
 * La funcion regresa TRUE cuando se dio de baja al alumno de forma exitosa.
 */

boolean unEnrollStudent(Student *student, Subject *subject) {

	if (subject->n == 0)
		return FALSE;
	else if (isStudentEnrolled(student->exp, subject)) {
		int j = 0; 		// Aux counter for dynamic array reshape.

		int newN = (subject->n) - 1;		// Update students counter
		subject->n = newN;

		/*Reserves new memory space*/
		Student **newMem = (Student**) malloc(newN * sizeof(Student*));

		/*Ciclo for itera con el valor previo de n (newN + 1)*/

		for (int i = 0; i < newN + 1; i++) {
			if ((subject->students_ptr[i]->exp) != student->exp) {
				/*copy 1 Student*'s from subject->students_ptr[i] to newMem[j]*/
				memcpy(newMem + j, &(subject->students_ptr[i]),
						sizeof(Student*));
				j++;
			}
		}

		/*Free old memory*/
		free(subject->students_ptr);

		/*Assign newMem into subject's dynamic array*/
		subject->students_ptr = newMem;

		/*Console validation*/
		printf("\n\n\tStudent '%s' removed successfully from subject %s!",
				student->name, subject->name);

		return TRUE;
	} else {
		return FALSE;
	}

}

boolean eraseSubject(Subject *subject) {
	printf("\n\n\tSubject '%s' removed successfully!",subject->name);
	free(subject->students_ptr);
	free(subject->name);
	free(subject);
	return TRUE;
}

void printStudents(Subject *subject) {
	int n = subject->n;

	printf("\n\n\tStudent list for subject '%s' contains %d members.",subject->name,n);
	for (int i = 0; i < n; i++) {
		printStudent(subject->students_ptr[i]);
	}

}
