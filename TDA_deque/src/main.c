#include <stdio.h>
#include <stdlib.h>
#include "Type.h"
#include "deque.h"

int main(void) {

	Type p = NULL;

	printf("\n\tTesting Queue \n");

	Deque q1;
	q1 = deque_create();
	for (int i = 20; i <= 30; i++) {
		deque_offer_front(q1, Int(i));
	}

	for (int i = 19; i >= 0; i--) {
			deque_offer_rear(q1, Int(i));
		}

	printf("Size (q1): %d \n", deque_size(q1));
	p = deque_peek_front(q1);
	printf("Front element in queue (q1): %d \n", *(int*) (p));

	p = deque_peek_rear(q1);
	printf("Rear element in queue (q1): %d \n", *(int*) (p));

	while ( TRUE != deque_isEmpty(q1)) {
		p = deque_poll_rear(q1);
		if (NULL != p) {
			printf(" %d ", *(int*) (p));
		}
	}

	deque_destroy(q1);
	printf("\nQueue (q1) has been destroyed ... \n");

	return 0;
}
