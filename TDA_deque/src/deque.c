#include "deque.h"
#include <stdlib.h>
#include <stdio.h>

/*********************************************************************************************************************/
/*Private data types definitions*/
/*********************************************************************************************************************/

struct strNode {
	Type data;					// Generic data type from local library "type.h"
	struct strNode *next;		// Next node pointer
	struct strNode *previous; 	// Previous node pointer
};

typedef struct strNode *Node;	// Node pointer definition

struct strDeque {
	Node front;					// Queue Top
	Node rear;					// Queue Bottom
	unsigned int size;			// Queue Size
	Boolean isEmpty;			// Empty flag.

};

/*********************************************************************************************************************/
/*Private functions implementation*/
/*********************************************************************************************************************/

Node create_node(Type data) {
	Node new = (Node) malloc(sizeof(struct strNode));
	new->next = NULL;
	new->previous = NULL;
	new->data = data;
	return new;
}

/*********************************************************************************************************************/
/*Public functions with private implementations.*/
/*********************************************************************************************************************/

Deque deque_create() {
	Deque DQ = (Deque) malloc(sizeof(struct strDeque));
	DQ->front = NULL;
	DQ->rear = NULL;
	DQ->isEmpty = TRUE;
	DQ->size = ZERO
	;
	return DQ;
}

void deque_offer_front(Deque DQ, Type element) {
	if (DQ) {
		/* Creates the node to be added at the front*/
		Node newNode = create_node(element);

		if (deque_isEmpty(DQ)) {
			/*Container is empty*/
			DQ->front = newNode;
			DQ->rear = newNode;
			DQ->isEmpty = FALSE;
		} else {
			DQ->front->previous = newNode;
			newNode->next = DQ->front;
			DQ->front = newNode;
		}
		DQ->size = DQ->size + 1;

	} else {
		printf(
				"\n\tUnable to add element to the front, container does not exist.");
	}

}

void deque_offer_rear(Deque DQ, Type element) {
	if (DQ) {
		/* Creates the node to be added at the front*/
		Node newNode = create_node(element);

		if (deque_isEmpty(DQ)) {
			/*Container is empty*/
			DQ->front = newNode;
			DQ->rear = newNode;
			DQ->isEmpty = FALSE;

		} else {
			DQ->rear->next = newNode;
			newNode->previous = DQ->rear;
			DQ->rear = newNode;
		}
		DQ->size = DQ->size + 1;

	} else {
		printf(
				"\n\tUnable to add element to the front, container does not exist.");
	}

}

unsigned int deque_size(Deque DQ) {
	return DQ->size;
}

Boolean deque_isEmpty(Deque DQ) {
	return DQ->isEmpty;
}

Type deque_poll_front(Deque DQ) {

	if (DQ) {
		if (deque_isEmpty(DQ)) {
			printf(
					"\n\tUnable to remove element at the front, queue is empty.");
			return NULL;
		} else {

			Node tempNode = DQ->front;
			Type tempElement = DQ->front->data;
			DQ->front = DQ->front->next;
			DQ->size = DQ->size - 1;
			if (ZERO == DQ->size) {
				DQ->isEmpty = TRUE;
				return tempElement;
			}
			free(tempNode);
			DQ->front->previous = NULL;

			return tempElement;
		}
	} else {
		printf(
				"\n\tUnable to add element to the front, container does not exist.");
		return NULL;

	}
}

Type deque_poll_rear(Deque DQ) {

	if (DQ) {
		if (deque_isEmpty(DQ)) {
			printf("\n\tUnable to remove element at the rear, queue is empty.");
			return NULL;
		} else {

			Node tempNode = DQ->rear;
			Type tempElement = DQ->rear->data;
			DQ->rear = DQ->rear->previous;
			DQ->size = DQ->size - 1;
			if (ZERO == DQ->size) {
				DQ->isEmpty = TRUE;
				return tempElement;
			}
			DQ->rear->next = NULL;
			free(tempNode);

			return tempElement;
		}
	} else {
		printf(
				"\n\tUnable to add element to the front, container does not exist.");
		return NULL;

	}
}

Type deque_peek_front(Deque DQ) {
	return DQ->front->data;
}

Type deque_peek_rear(Deque DQ) {
	return DQ->rear->data;
}
/* Function: Destroy container.
 * Doc prototype: destroyDeque(Deque);
 * Prototype: void deque_destroy(Deque);
 * Input: Deque.
 * Description:
 * -	Runs an algorithm to empty all nodes, freeing each one of them.
 * -	Proceeds to free the reserved space for the container.
 * Returns: void function.
 */
void deque_destroy(Deque DQ) {
	while ( TRUE != deque_isEmpty(DQ)) {
		deque_poll_front(DQ);
	}
	free(DQ);
}
