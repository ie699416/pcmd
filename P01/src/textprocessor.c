#include "textprocessor.h"

void getPath(char *pathPtr) {
	printf("\nEnter file path:");
	scanf("%s", pathPtr);
}

void addEOS(MMU *MMU_instance) {
	char eos = '\0';
	Type *eosPtr = (Type) &eos;
	MMU_addElement(eosPtr, MMU_instance);
}

void printBufferIndex(char *buffer, unsigned int n) {
	printf("\n\nRead elements: \n");
	for (int i = 0; i < n; i++) {
		printf("\nValue : %c", buffer[i]);
		printf(" | Index : %d ", i);
	}
}

void printBufferText(char *buffer, unsigned int n) {
	printf("\n\nRead elements: \n");
	for (int i = 0; i < n; i++) {
		printf("%c", buffer[i]);
	}
}

int printMenu(char *buffer, unsigned int n) {
	unsigned int menu_option = 0;
	unsigned int wait = 0;

	printf("\n\n\t Menu options :");
	printf("\n\t\t- [1] Total words in file.");
	printf("\n\t\t- [2] Number of sentences.");
	printf("\n\t\t- [3] Sort sentences A-Z.");
	printf("\n\t\t- [4] Sort sentences Z-A.");
	printf("\n\t\t- [5] Sort sentences larger to shorter.");
	printf("\n\t\t- [6] Sort sentences shorter to larger.");
	printf("\n\t\t- [7] Top ten famous words.");
	printf("\n\t\t- [8] Show original text.");
	printf("\n\t\t- [8] Show original text.");
	printf("\n\t\t- [9] Capture another PATH.");
	printf("\n\t\t- [0] Exit.");

	scanf("%d", &menu_option);

	switch (menu_option) {
	case 0:
		return ERR;

		break;
	case 1:
		menu_1(buffer, n);
		break;

	case 2:
		menu_2(buffer, n);
		break;

	case 3:
		menu_3(buffer, n);
		break;

	case 4:
		menu_4(buffer, n);
		break;

	case 5:
		menu_5(buffer, n);
		break;

	case 6:
		menu_6(buffer, n);
		break;

	case 7:
		menu_7(buffer, n);
		break;

	case 8:
		menu_8(buffer, n);
		break;

	case 9:
		return TRUE;
		break;

	default:
		return ZERO;
	}
	printf("\n\t\t Continue?");
	printf("\n\t\t- [0] No (exit)");
	printf("\n\t\t- [1] Yes (Get back to main menu)");

	while (1) {
		scanf("%d", &wait);
		switch (wait) {
		case 0:
			return ERR;
		case 1:
			return ZERO;
		default:
			continue;
		}
	}
	return TRUE;
}

//Upper case: between 65 and 90 ;  Lower case: between 97 and 122
boolean isLetter(char *value) {
	if ((65 <= *value && 90 >= *value) || (97 <= *value && 122 >= *value)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

boolean isSpace(char *value) {
	return (32 == *value) ? TRUE : FALSE;
}

boolean isPeriod(char *value) {
	return (46 == *value) ? TRUE : FALSE;
}

unsigned int countWords(char *buffer, unsigned int n) {
	unsigned int words = 0;
	boolean isLetter_flag = FALSE;
	boolean wasLetter_flag = FALSE;
	char value;

	for (int i = ZERO; i < n; i++) {
		wasLetter_flag = isLetter_flag; //keeps previous value
		value = buffer[i];
		isLetter_flag = isLetter(&value);

		/*To find a word, we need to make sure the previous value was a letter and the current one is not*/
		if (FALSE == isLetter_flag && TRUE == wasLetter_flag)
			words = words + 1;
	}
	return words;
}

unsigned int countSentences(char *buffer, unsigned int n) {
	unsigned int sentences = 0;
	boolean isLetter_flag = FALSE;
	boolean wasLetter_flag = FALSE;
	boolean isPeriod_flag = FALSE;
	char value;

	for (int i = ZERO; i < n; i++) {
		wasLetter_flag = isLetter_flag; //keeps previous value
		value = buffer[i];
		isPeriod_flag = isPeriod(&value);
		isLetter_flag = isLetter(&value);

		/*To find a word, we need to make sure the previous value was a letter and the current one is not*/
		if (TRUE == wasLetter_flag && TRUE == isPeriod_flag)
			sentences = sentences + 1;
	}
	return sentences;
}

void famousTen(char *buffer, unsigned int n) {
	char value;

	int f = 9;
	int g = 0;
	int j = 0;
	int i = 0;
	boolean isLetter_flag = FALSE;
	boolean wasLetter_flag = FALSE;
	boolean equals = FALSE;
	Word famousTenArray[10];
	for (int j = 0; j < 20; j++) {
		famousTenArray[0].word[j] = ZERO;
		famousTenArray[1].word[j] = ZERO;
		famousTenArray[2].word[j] = ZERO;
		famousTenArray[3].word[j] = ZERO;
		famousTenArray[4].word[j] = ZERO;
		famousTenArray[5].word[j] = ZERO;
		famousTenArray[6].word[j] = ZERO;
		famousTenArray[7].word[j] = ZERO;
		famousTenArray[8].word[j] = ZERO;
		famousTenArray[9].word[j] = ZERO;
	}

	famousTenArray[0].length = ZERO;
	famousTenArray[1].length = ZERO;
	famousTenArray[2].length = ZERO;
	famousTenArray[3].length = ZERO;
	famousTenArray[4].length = ZERO;
	famousTenArray[5].length = ZERO;
	famousTenArray[6].length = ZERO;
	famousTenArray[7].length = ZERO;
	famousTenArray[8].length = ZERO;
	famousTenArray[9].length = ZERO;

	unsigned int words = countWords(buffer, n);
	unsigned int lettersIndex;
	char tempWord[20];
	for (j = 0; j < 20; j++) {
		tempWord[j] = ZERO;
	}
	lettersIndex = ZERO;

	for (j = 0; j < 20; j++) {
		tempWord[j] = ZERO;
	}
	lettersIndex = ZERO;

	for (i = ZERO; i < n; i++) {
		value = buffer[i];
		wasLetter_flag = isLetter_flag; //keeps previous value
		isLetter_flag = isLetter(&value);

		if (FALSE == wasLetter_flag && TRUE == isLetter_flag) {
			tempWord[lettersIndex] = value;
			lettersIndex = 1;
		} else if (TRUE == wasLetter_flag && TRUE == isLetter_flag) {
			tempWord[lettersIndex] = value;
			lettersIndex = lettersIndex + 1;
		}
		/*To find a word, we need to make sure the previous value was a letter and the current one is not*/
		else if (TRUE == wasLetter_flag && FALSE == isLetter_flag) {
			printf("\n Word: %s | Length: %d", tempWord, lettersIndex);

			/*Compare words and save*/
			for (f = 0; f < 10; f++) {
				if (ZERO == famousTenArray[f].length) {
					famousTenArray[f].length = lettersIndex;
					for (g = ZERO; g < famousTenArray[f].length; g++) {
						famousTenArray[f].word[g] = tempWord[g];
					}
					break;

				} else {
					if (lettersIndex == famousTenArray[f].length) {

						for (j = 0; j < lettersIndex; j++) {
							equals =
									(tempWord[j] == famousTenArray[f].word[j]) ?
											TRUE : FALSE;
						}
						if (equals) {
							printf("\tSame words at index rank [%d]", f);
							/*Swap positions*/
							break;
						}
					}
				}
			}

			/*Reset tempWord array*/
			for (j = 0; j < 20; j++) {
				tempWord[j] = ZERO;
			}
			lettersIndex = ZERO;

			words = words + 1;
		} else {
			lettersIndex = ZERO;
		}
	}
}

void menu_1(char *buffer, unsigned int n) {
	unsigned int words = countWords(buffer, n);
	printf("\n\n\tTotal number of words in file: %d\n", words);
}

void menu_2(char *buffer, unsigned int n) {
	unsigned int sentences = countSentences(buffer, n);
	printf("\n\n\tTotal number of sentences in file: %d\n", sentences);

}

void menu_3(char *buffer, unsigned int n) {

}

void menu_4(char *buffer, unsigned int n) {

}

void menu_5(char *buffer, unsigned int n) {

}

void menu_6(char *buffer, unsigned int n) {

}

void menu_7(char *buffer, unsigned int n) {
	famousTen(buffer, n);
}

void menu_8(char *buffer, unsigned int n) {
	printBufferText(buffer, n);
}

