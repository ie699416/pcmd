/*
 ============================================================================

 ============================================================================
 */

#include "filereader.h"
#include "textprocessor.h"

int main(void) {
	/*Eclipse buffers flush commands*/
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);

	/*Variables declaration*/
	char path[_MAX_PATH];	// Windows PATH limit is 260 characters
	boolean fileExist;		// File read flag
	MMU *MMU_instance;		// MMU instance reference
	int menuOP;

	/*Init variables.*/
	MMU_instance = MMU_newInstance();
	menuOP = ZERO;

	/*User Interface*/
	while (1) { 			// Get path Infinite loop
		MMU_clean(MMU_instance);
		getPath(path);
		fileExist = FR_readFile(path, MMU_instance);

		if (fileExist) {

			int n = MMU_instance->numel;
			char buffer[n]; // Static array declaration with variable length 'n' as copy of the current memory.
			MMU_readMemory(MMU_instance, n, buffer, FIFO); // Copies memory data into buffer.

			while (1) { 			// Menu Infinite loop
				menuOP = printMenu(buffer,n);
				if (ERR == menuOP)
					return 0;
				else if (ZERO == menuOP) {
					continue;
				} else {
					break;
				}
			}

		} else {
			printf("\n Invalid File.");
		}

	}

	/*Dynamic Memory release*/
	MMU_destroyInstance(MMU_instance);
	return 0;
}
