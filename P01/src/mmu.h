/*********************************************************************************************************************/

#ifndef MMU_H_
#define MMU_H_

#include <stdio.h>
#include <stdlib.h>

#define FALSE 				  0
#define TRUE				  1

#define	FIFO				  0
#define LIFO				  1

#define ERR					 -1

#define MEMORY_SIZE 	1048576					// 1MB
#define ZERO				  0


/*********************************************************************************************************************/
/*ADT definitions*/

typedef enum {false, true} boolean; 			// Size 4 Bytes

typedef void * Type; 							// Generic pointer type

typedef struct strMMU {
	Type startPtr;								// Address at the start of the reserved heap.
	Type addresses [MEMORY_SIZE];				// Array of heap addresses in use.
	Type * freeMemoryPtr;						// Address on which free memory is available.
	unsigned int numel;							// Number of bytes stored.
	boolean isFull;								// When TRUE, no memory is left.
	boolean isEmpty;							// When TRUE, all memory is available.
} MMU;
/*********************************************************************************************************************/


/* Function: Create new MMU instance
 * Prototype: MMU * MMU_newInstance(void);
 * Input: void
 * Description:
 * -	The MMU instance is created with MALLOC.
 *
 * -	The InstanceMMU members are initialized as:
 *
 * 		MMU.startPtr: MEMORY_SIZE Bytes are reserved at the heap through the
 * 		MALLOC function and the heap address is pointed by MMU.startPtr.
 *
 * 		MMU.addresses: An static array of addresses of size MEMORY_SIZE is
 * 		initialized	with each element pointing to NULL.
 *
 * 		MMU.freeMemoryPtr: As all locations are available, freeMemoryPtr
 * 		points to InstanceMMU.addresses[0].
 *
 * 		MMU.numel:	Set to 0 as all addresses are available.
 *
 * 		MMU.isFull: Set to FALSE.
 *
 * 		MMU.isEmpty: Set to TRUE.
 *
 * 	Returns: MMU address.
 */

MMU * MMU_newInstance(void);

/* Function: Destroy MMU Instance
 * Prototype: void MMU_destroyInstance(MMU *);
 * Input: MMU*
 * Description:
 * -	To destroy the MMU Instance, all elements at the heap need to be released
 * 		through the FREE function.
 *
 * -	The MMU Instance address is freed as well as it was created dynamically.
 *
 * Returns: Void.
 */

void MMU_destroyInstance(MMU *);

/* Function: Increase and check if Full.
 * Prototype: void MMU_increaseAndCheck(MMU *);
 * Input: MMU*
 * Description:
 * -	Simply increases the value of MMU.numel by one element.
 * -	Set the value of MMU.freeMemoryPtr to the next location at MMU.addresses.
 * 			>>> freeMemoryPtr = ++freeMemoryPtr;
 * -	If the value of MMU.numel equals MEMORY_SIZE, isFull is set to TRUE.
 * Returns: void.
 */

void MMU_increaseAndCheck(MMU *);

/* Function: Decrease and check if Empty.
 * Prototype: void MMU_decreaseAndCheck(MMU *);
 * Input: (MMU *)
 * Description:
 * -	Simply decreases the value of MMU.numel by one element.
 * -	Set the value of MMU.freeMemoryPtr to the previous location at MMU.addresses.
 * 			>>> freeMemoryPtr = --freeMemoryPtr;
 * -	If the value of MMU.numel equals ZERO, isEmpty is set to TRUE.
 * Returns: void.
 */

void MMU_decreaseAndCheck(MMU *);

/* Function: Add Element
 * Prototype: boolean MMU_addElement(Type, MMU *);
 * Input:  Type, MMU *
 *****************************************************************************
 * NOTE: The function does not need to return the index on which the value was
 * stored as the elements are added with a LiFo policy. Therefore the index
 * will reference the current value through MMU.numel.
 *****************************************************************************
 * Description:
 * -	Checks if MMU.isFull is FALSE to continue.
 * 			>> Else, returns FALSE (as the function can't add any other element).
 *
 * -	When adding a new element into the reserved heap the MMU stores the element
 * 		into the next available address pointed by freeMemoryPtr.
 *
 * -	The referenced value is stored at the index MMU.addresses[MMU.numel];
 *
 * -	The MMU_increaseAndCheck function is called with the MMU.self reference.
 *
 * -	Returns TRUE if the value was allocated correctly.
 *
 * Returns:	boolean
 */

boolean MMU_addElement(Type, MMU * );

/* Function: Remove Element
 * Prototype: boolean MMU_removeElement(Type element, MMU *);
 * Input: Type, MMU *
 *****************************************************************************
 * NOTE: The function does not need to receive the index on which the value is
 * stored as the elements are removed with a LiFo policy. Therefore the index
 * will be hold with the current value at MMU.numel.
 *****************************************************************************
 * Description:
 *
 * -	Checks if MMU.isEmpty is FALSE to continue.
 * 			>> Else, returns FALSE (as the function can't remove any other element).
 *
 * -	Access the value as an index with the current value of MMU.numel
 * 		as the buffer is a static array of addresses.
 *
 * -	The value is copied to the reference of the input argument type "Type".
 *
 * -	Proceeds to "free" the corresponding value making the address point to NULL.
 *
 * -	Calls the MMU_decreaseAndCheck Function.
 * Returns: boolean
 */

boolean MMU_removeElement(Type, MMU *);

/* Function: Getter isFull Flag.
 * Prototype: boolean MMU_getIsFull(MMU*);
 * Input: MMU *
 * Description: Returns value of isFull flag.
 * Returns: boolean
 */

boolean MMU_getIsFull(MMU*);

/* Function: Getter isEmpty Flag.
 * Prototype: boolean MMU_getIsEmpty(MMU *);
 * Input: MMU *
 * Description: Returns value of isEmpty flag.
 * Returns: boolean
 */

boolean MMU_getIsEmpty(MMU*);


/* Function: Calculate Used space available.
 * Prototype: float MMU_calculateUseRatio(unsigned int);
 * Input: unsigned int
 * Description:
 * -	Input argument is a length in bytes.
 * -	Simply provides the ratio between bytes / MEMORY_SIZE
 * Returns: float
 */

float MMU_calculateUseRatio(unsigned int);

/* Function: Check if the elements fit in the remaining space
 * Prototype: boolean MMU_checkFit(char *, unsigned int, MMU *);
 * Input: char *, unsigned int, MMU *
 * Description:
 * -	Function request the String and length so it can verify if the
 * 		required space is available.
 * -	Calls the function MMU_calculateUseRatio() and returns boolean flag.
 * Returns: boolean
 */

boolean MMU_checkFit(unsigned int, MMU *);

/* Function: Print Memory Elements
 * Prototype: void MMU_printElements(MMU *);
 * Input: MMU *
 * Description:
 * -	Using PRINTF prints the concatenaded values at the memory between
 * 		ZERO and MMU.numel;
 * Returns: void
 */

void MMU_printElements(MMU *);

/* Function: Read Index
 * Prototype: char * MMU_readByte(MMU *, unsigned int);
 * Input: MMU *, unsigned int
 * Description:
 * -	Returns the referenced value at a given index.
 * Returns: char *
 */

char * MMU_readIndex(MMU* , unsigned int );

/* Function: Read Memory Elements
 * Prototype: void MMU_readMemory(MMU *,unsigned int , char*,boolean);
 * Input: MMU *, unsigned int , char*, boolean
 * Description:
 * -	Calls MMU_readIndex() with a iterative cycle.
 * -	Returns by reference an array of chars of length MMU..
 * -	The array can be returned with a LIFO or FIFO order with the second arg.
 * Returns: void
 */

void MMU_readMemory(MMU *,unsigned int , char*,boolean);


/* Function: Write String
 * Prototype: unsigned int MMU_writeString(char*, unsigned int, MMU *);
 * Input: char *, unsigned int, MMU *
 * Description:
 * -	Before writing a string, the function calls MMU_checkFit()
 * 		>> returns -1 (ERR) if MMU_checkFit returns FALSE;
 * -	The function expects a string to be written to the memory.
 * -	The memory will have the string end character '\0' at the end
 * 		of each sentence (string) so it can identify the different lengths
 * 		in use.
 * 		>> returns the virtual index on which the sentence got written.
 * Returns: unsigned int.
 */

int MMU_writeString(MMU* , unsigned int , char* );

/* Function: clean memory
 * Prototype: void MMU_clean(MMU *);
 * Input: MMU *
 * Description:
 * -	Simply makes all addresses point to NULL and resets flags values.
 * -	MMU.numel is set to 0.
 * Returns: void
 */

void MMU_clean(MMU *);


#endif /* MMU_H_ */
