
#ifndef TEXTPROCESSOR_H_
#define TEXTPROCESSOR_H_

#include "mmu.h"
#include <string.h>


typedef struct str_word{
	unsigned int length;	// Size in Bytes.
	unsigned int count;		// Histogram count
	char word [20];			// Self reference to the word with fixed max length 20.
}Word;


typedef struct str_sentence{
	unsigned int words;		// Number of words in the sentences.
	unsigned int size;		// Size in Bytes or chars.
	char * sentence;		// Self reference to the sentence string.
}Sentence;

/* Function: Print menu
 * Prototype: void printMenu(void);
 * Input: MMU *instance
 * Description: Prints the 8 options and calls scanf() to wait for the user input.
 * Returns: integer
 */

int printMenu(char * buffer, unsigned int n);

/* Function: Print Index
 * Prototype: void printBufferIndex(char *, unsigned int);
 * Input: char *, unsigned int
 * Description:  Prints the buffer elements starting at zero until n, where n is expected to be the buffer size.
 * Returns: void
 */

void printBufferIndex(char *, unsigned int );

/* Function: Print Index
 * Prototype: void printBufferText(char *, unsigned int);
 * Input: char *, unsigned int
 * Description:  Prints the buffer string.
 * Returns: void
 */

void printBufferText(char *, unsigned int);


/* Function: Add End Of String character.
 * Prototype: void addEOS(MMU *);
 * Input: MMU *
 * Description: calls MMU_addElement with the reference of a character '\0' to incorporate into the memory.
 * Returns: void
 */

void addEOS(MMU *);

/* Function: Capture text PATH:
 * Prototype: void getPath(char * );
 * Input: char *
 * Description: This function makes use of the scanf() function to ask the user for a path.
 * Returns: void
 */

void getPath(char * );

/* Function: Count words
 * Prototype: unsigned int countWords(char * , unsigned int );
 * Input: char * , unsigned int*
 * Description:
 * -	To find a word, we need to make sure the previous value was a letter and the current one is not
 * Returns: unsigned int
 */

unsigned int countWords(char * , unsigned int );

/* Function: Count sentences
 * Prototype: unsigned int countSentences(char * , unsigned int );
 * Input: char * , unsigned int*
 * Description:
 * -	To find a sentences, we need to make sure the previous value was a letter and the current one is a period.
 * Returns: unsigned int
 */

unsigned int countWords(char * , unsigned int );

/* Function: Check if char correspond to an alphabet symbol.
 * Prototype: boolean isLetter(char * );
 * Input: char *
 * Description:
 * -	This function compares the input and returns true if is the value is different
 * 		from the ASCII values corresponding to the alphabet characters (upper case and lower case)
 * 		>> Upper case: between 65 and 90
 * 		>> Lower case: between 97 and 122
 * Returns: boolean
 */

boolean isLetter(char * );

/* Function: Check if char correspond to space
 * Prototype: boolean isSpace(char * );
 * Input: char *
 * Description: This function compares the input and returns true if is equal to 32. (' ').
 * Returns: boolean
 */

boolean isSpace(char * );


/* Function: Check if char correspond to a period
 * Prototype: boolean isSpace(char * );
 * Input: char *
 * Description: This function compares the input and returns true if is equal to 46. ('.').
 * Returns: boolean
 */

boolean isPeriod(char * );


/* Function: Famous ten
 * Prototype: void famousTen(char *, unsigned int);
 * Input: char *, unsigned int
 * Description:
 * -	This function keeps a histogram track of the top ten famous words.
 *
 * -	As it has to evaluate all words, it needs to know how many words are in the text.
 *
 * -	When the iterative cycle finds a word, it will compare it with the top ten, by length. *
 * 		>>	If the length of the word, matches the length of any at the top ten it will now proceed
 * 			to evaluate all the elements as a bitwise operation.
 * 			>> If the bitwise AND returns true, the famous top ten will be updated.
 *
 *
 * Returns: boolean
 */

void famousTen(char *, unsigned int);

/* Function: [1] Total words in file:
 * Prototype: void menu_1 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * - 	This case counts the words separated by any special character.
 *  		>>Examples of special characters:   !"\#$%&'()*+,-.:;<=>?@[\\]^_`{|}~ ...
 * -	By reading all indexes within ZERO and a counter will increase when the index
 * Returns: void
 */



void menu_1 (char * , unsigned int );
/* Function: [2] Number of sentences separated by dots.
 * Prototype:void menu_2 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */

void menu_2 (char * , unsigned int );

/* Function: [3] Sort text sentences A-Z
 * Prototype:void menu_3 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -	.
 * Returns: void
 */

void menu_3 (char * , unsigned int );

/* Function: [4] Sort text sentences Z-A.
 * Prototype: void menu_4 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */

void menu_4 (char * , unsigned int );

/* Function: [5] Sort text sentences larger to shorter.
 * Prototype: void menu_5 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */

void menu_5 (char * , unsigned int );

/* Function: [6] Sort text sentences shorter to larger.
 * Prototype: void menu_6 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */

void menu_6 (char * , unsigned int );
/* Function: [7] Top ten famous words.
 * Prototype: void menu_7 (char * , unsigned int );
 * Input: char * , unsigned int
 * Description:
 * -
 * Returns: void
 */

void menu_7 (char * , unsigned int );

/* Function: [8] Show original text.
 * Prototype: void menu_8(char * , unsigned int );
 * Input: char * , unsigned int
 * Description: Executes the algorithm to print the original text.
 * Returns: void
 */

void menu_8 (char * , unsigned int );

#endif /* TEXTPROCESSOR_H_ */
