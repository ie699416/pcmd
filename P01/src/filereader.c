#include "filereader.h"

boolean FR_readFile(char *pathPtr, MMU *instance) {
	char *temp = pathPtr;
	while (*temp != '\0') {
		printf("%c", *temp);
		temp++;
	}
	printf("\n\n");
	FILE *f = fopen(pathPtr, "r");

	if (f == NULL) {
		printf("\n\tError: File could not be opened.\n");
		fclose(f);
		return FALSE;
	} else {
		fseek(f, 0, SEEK_END);
		long nBytes = ftell(f);
		rewind(f);
		printf("\n\tBytes : %ld\n", nBytes);
		char c;
		int i = 0;
		printf("\t");
		while (i < nBytes && i < MEMORY_SIZE) {
			c = fgetc(f);
			printf("%c", c);
			MMU_addElement(&c, instance);
			i++;
		}
		printf("\nBytes stored: %d", instance->numel);
		fclose(f);
		return TRUE;
	}
}
