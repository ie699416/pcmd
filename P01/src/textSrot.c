#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void quicksort(char **array, int first_line, int last_line);

int main1(int argc, char *argv[]) {
	int i = 0;
	int j = 0;
	int line_count = 0;
	int line_number = 20;
	char *lines[100];
	FILE *foutptr;
	FILE *finptr;

	finptr = fopen("C:\\699416\\PcMD\\P01\\src\\names.txt", "r");
	if (finptr == NULL) {
		perror("ERROR: input file pointer");
		return 1;
	}

	foutptr = fopen("sorted.txt", "w");
	if (foutptr == NULL) {
		perror("ERROR: output file pointer");
		return 1;
	}

	lines[i] = malloc(BUFSIZ);
	while (fgets(lines[i], BUFSIZ, finptr) && i < line_number) {
		printf("Element %d is the name %s.\n", i, lines[i]);
		i++;
		lines[i] = malloc(BUFSIZ);
	}

	printf("TEST %s.\n", *(lines + 1));
	line_count = i;
	i = 0;
	char **all_lines = lines;

	quicksort(all_lines, 0, line_count - 1);

	for (j = 0; j < line_count; j++) {
		fprintf(foutptr, "%s", lines[j]);
	}

	printf("\n\tArray[%d]: %s", 0, all_lines[0]);
	printf("\n\tArray[%d]: %s", 1, all_lines[1]);
	printf("\n\tArray[%d]: %s", 2, all_lines[2]);
	printf("\n\tArray[%d]: %s", 3, all_lines[3]);
	printf("\n\tArray[%d]: %s", 4, all_lines[4]);
	return 0;
}

void quicksort(char **array, int first_line, int last_line) {
	int i = 0, j = 0, x = 0i, p = (first_line + last_line) / 2;
	printf("%s \n", array[p]);
	char pivot[BUFSIZ];
	char *temp_ptr;

	strcpy(pivot, array[p]);

	i = first_line;
	j = last_line;

	if (last_line - first_line < 1) {
		return;
	}

	printf(
			"first_line = %d so i = %d. last line = %d so  j = %d. The pivot is array[%d] \n",
			first_line, i, last_line, j, p);

	while (i <= j) {
		/* While array[i] proceeds the pivot alphabetically and is less than array[j] move through the array. This deals with first
		 mismatching letter of first string is less than the second in ASCII character numbers i.e. if earlier in the alphabet as ASCII
		 chartcters earlier in the alphabet have a lower numerical id */
		while (strcmp(array[i], pivot) < 0 && i < last_line) {
			printf(
					"No swapping In while 1, comparing %s and %s with strcmp = %d. \n",
					array[i], pivot, strcmp(array[i], pivot));
			printf("%d. \n", i);
			i++;
		}

		/* While array[j] follows the pivot alphabetically and is
		 greater than array[i] move through the array. This deals with the
		 first mismatching letter of first string compared to the second
		 is greater in number id i.e. the ASCII character code is
		 bigger in the first so it is  latter in the alphabet */
		while (strcmp(array[j], pivot) > 0 && j > first_line) {
			printf(
					"No swapping In while 2, comparing %s and %s with strcmp = %d. \n",
					array[j], pivot, strcmp(array[j], pivot));
			printf("%d. \n", j);
			j--;
		}


		// Swap if the conditions above are not met
		if (i <= j) {
			printf("%s\n", "SWAPPING");
			printf("Array[%d] = %s, Array[%d] = %s", i, array[i], j, array[j]);
			temp_ptr = array[i]; //Here we copy pointers which is eaier to do than copy the array contents and is more efficent.
			array[i] = array[j];
			array[j] = temp_ptr;
			//strcpy(temp,array[i]) ; // This is much less efficent as it copies the contents of the aray not the pointers.
			//strcpy(array[i], array[j]) ;
			//strcpy(array[j],temp) ;
			printf("Array[%d] = %s, Array[%d] = %s", i, array[i], j, array[j]);
			i++;
			j--;
		}
	}

	if (first_line < j) {
		printf("Recursive call 1: Lower bound = %d, Upper bound = %d\n",
				first_line, j);
		for (x = 0; x < last_line + 1; x++) {
			printf("Array[%d]: %s \n", x, array[x]);
		}
		quicksort(array, first_line, j);
	}

	x = 0;

	if (i < last_line) {
		printf("Recursive call 2: Lower bound = %d, Upper bound = %d\n", i,
				last_line);
		for (x = 0; x < last_line + 1; x++) {
			printf("Array[%d]: %s \n", x, array[x]);
		}
		quicksort(array, i, last_line);
	}

	x = 0;
	return;
}
