/*
 * Main.c
 *
 *  Created on: 26/04/2016
 *      Author: IE699416
 */

#include "stack.h"
#include <stdio.h>
#include <stdlib.h>

int main() {

	char expression[] = "4 * (a + b)";
	puts(expression);
	isWellBalanced(expression);

	char e2[] = "4 * (a + b))";
	puts(e2);
	isWellBalanced(e2);

	char e3[] = "15 / (3 * (b + c))";
	puts(e3);
	isWellBalanced(e3);

	char e4[] = "15 / ((3 * (b + c))";
	puts(e4);
	isWellBalanced(e4);

	char e5[] = "(x + 2) / (3 * b)";
	puts(e5);
	isWellBalanced(e5);

	char e6[] = "(x + 2)) / (3 * b)";
	puts(e6);
	isWellBalanced(e6);

	return 0;
}
