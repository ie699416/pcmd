

#ifndef STACK_H_
#define STACK_H_

#include "type.h"

typedef void* Type;
typedef struct strStack* Stack;

Stack stack_create();

void  stack_destroy(Stack);

void  stack_push(Stack, Type);

int   stack_size(Stack);

Boolean  stack_isEmpty(Stack);

Type  stack_top(Stack);

Type  stack_pop(Stack);

void  isWellBalanced (char * s);


#endif /* STACK_H_ */
