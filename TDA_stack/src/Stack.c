
#include "Stack.h"
#include <stdlib.h>
#include <stdio.h>

struct strNode {
	Type data;
	struct strNode* prior;
};

typedef struct strNode* Node;

struct strStack {
	Node top;
	int size;
};

unsigned int len(char string[]) {
	char * p;
	p = string;
	while (*p != '\0') {
		p++;
	}
	return p - string;
}

Stack stack_create() {
	Stack s = (Stack) malloc(sizeof(struct strStack));
	s->size = 0;
	s->top = NULL;
	return s;
}

void stack_destroy(Stack s) {
	Node current = s->top;
	if (!stack_isEmpty(s)) {
		Node prior;
		while (current->prior != NULL) {
			prior = current->prior;
			free(current);
			current = prior;
		}
		free(current);
	}
	free(s);
}

void stack_push(Stack s, Type t) {
	Node n = (Node) malloc(sizeof(struct strNode));
	n->data = t;
	if (s->size == 0) {
		n->prior = NULL;
		s->top = n;
	} else {
		n->prior = s->top;
		s->top = n;
	}
	s->size++;
}

int stack_size(Stack s) {
	return s->size;
}

Boolean stack_isEmpty(Stack s) {
	if(s->top == NULL)
		return TRUE;
	else
		return FALSE;
}

Type stack_top(Stack s) {
	if (stack_isEmpty(s))
		return NULL;
	return s->top->data;
}

Type stack_pop(Stack s) {
	if (stack_isEmpty(s))
		return NULL;
	Node top = s->top;
	Type data = top->data;
	s->top = top->prior;
	free(top);
	s->size--;
	return data;
}

void isWellBalanced(char * e) {
	int i;
	Stack balanced = stack_create();

	for (i = 0; i < len(e); i++) {
		if (e[i] == 40) {
			if (stack_isEmpty(balanced)) {
				stack_push(balanced, "(");
			} else {
				stack_push(balanced, "(");
			}
		} else if (e[i] == 41) {
			if (stack_isEmpty(balanced)) {
				printf("Not balanced \n");
				stack_destroy(balanced);
				return;
			} else {
				stack_pop(balanced);
			}
		}
	}

	if (stack_isEmpty(balanced)) {
		printf("Balanced\n");

	} else if (stack_top(balanced)) {
		printf("Not balanced\n");
	}
	stack_destroy(balanced);

}

