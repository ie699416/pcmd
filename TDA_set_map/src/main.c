//
//  main.c
//  Set_xcode
//
//  Created by JFCA on 23/04/20.
//  Copyright © 2020 personal. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "Type.h"
#include "Set.h"
#include "Map.h"
#include <ctype.h>

typedef struct {
	float x, y, z;
} Vertex;

/* Para generar indices usando Datos de entrada:
 - ALFABETO contiene los caracteres que se pueden utilizar para formar un Key válido
 - M es el limite superior del rango de indices que queremos (0, M-1) para la tabla Hash
 - Key es la llave que la funcion hash procesara� para generar el Indice.
 Datos de salida:
 - Index que indica la posicion en la tabla Hash donde debera almacenar la dupla <Key, Data>
 */


unsigned int binary_hash(unsigned int, char*);

unsigned int hash(char *alphabet, unsigned int m, char *key);

unsigned int hash2(char *alphabet, char *key, unsigned int m);

//En este ejemplo, valueOf simplemente se retorna el índice
//donde se encuentra el caracter en el alfabeto
unsigned int valueOf(char character, char *alphabet);

unsigned int hashFunction(Type key, unsigned int size);

int cmpData(Type d1, Type d2);

Vertex* newVertex(float x, float y, float z);

int compareInt(Type k1, Type k2);

void PrintIntKey(Type key);

typedef struct {

	char title[100];

	char content[5000];

	char author[45];

} Book;

int cmp_book(Book b1, Book b2);

unsigned int hash_problema1(char *key) {
	int m = 100;
	char alphabet[] = "ABCDEFGHIJKLMN�OPQRSTUVWXYZ";
	unsigned int index = 0;
	int base = strlen(alphabet);   //longitud del alfabeto
	int lk = strlen(key);        //longitud de la llave

	//Horner
	int i = 0;
	index = valueOf(key[i], alphabet) % m;
	//Mientras existan caracteres en key por procesar,
	while (i < lk - 1) {
		index = ((index * base) % m) + (valueOf(key[i], alphabet) % m);
		i++;
	}
	return index;
}


int main() {

	unsigned int index;

	printf("Hash(110101010101010101001010101011111111111111100001): %u \n",
			index);
	index = hash_problema1("P100");

	printf("Hash(P100): %u \n", index);
	index = hash_problema1("R10");
	printf("Hash(R10): %u \n", index);
	index = hash_problema1("L23");
	printf("Hash(L23): %u \n", index);
	index = hash_problema1("P53");
	printf("Hash(P53): %u \n", index);
	index = hash_problema1("A54");
	printf("Hash(A54): %u \n", index);
	index = hash_problema1("D87");
	printf("Hash(D87): %u \n", index);

//	Map m1 = map_create(50, 0.9, 0.5, hashFunction, cmpData);
//
//	unsigned int k;
//	Vertex *v1 = newVertex(1.0, 3.0, 4.5);
//	map_set(m1, Int(0), (Type) v1);
//
//	Vertex *v2 = newVertex(2.0, 3.0, 4.5);
//	map_set(m1, Int(1), (Type) v2);
//
//	Vertex *v3 = newVertex(3.0, 3.0, 4.5);
//	map_set(m1, Int(4), (Type) v3);
//
//	k = 1;
//	Vertex *tmp = (Vertex*) map_get(m1, (Type) &k);
//	printf("%.2f , %.2f , %.2f \n", tmp->x, tmp->y, tmp->z);
//	printf("Fin de map ...\n");
//
//	Set s1;
//	s1 = set_create(compareInt);
//	printf("# de elementos en el contenedor: %d \n", set_size(s1));
//	printf("Agregando elementos al contenedor Set ... \n");
//	set_add(s1, Int(5));
//	set_add(s1, Int(3));
//	set_add(s1, Int(4));
//	set_add(s1, Int(2));
//	set_add(s1, Int(1));
//	set_add(s1, Int(8));
//	set_add(s1, Int(9));
//	set_add(s1, Int(6));
//	set_add(s1, Int(7));
//	set_add(s1, Int(10));
//	set_add(s1, Int(0));
//
//	printf("# de elementos en el contenedor: %d \n", set_size(s1));
//	printf("Contenido del contenedor (inorden): \n");
//	set_print(s1, PrintIntKey);
//
//	printf("\nEliminando un elemento del contendor Set ... \n");
//	set_remove(s1, Int(1));
//
//	printf("# de elementos en el contenedor: %d \n", set_size(s1));
//
//	printf("Contenido del contenedor (inorden): \n");
//	set_print(s1, PrintIntKey);
//	printf("\nFin de set ... \n");
//
//	char alphabet_1[] = "01";
//	unsigned int m = 555;
//	printf("\nAlphabet: %s\n", alphabet_1);
//	printf("Rango de indices: (0 - %d )\n", m - 1);
//
//	unsigned int index;
//
//	index = binary_hash( m,
//			"110101010101010101001010101011111111111111100001");
//	printf("Hash(110101010101010101001010101011111111111111100001): %u \n",
//			index);
//
//	index = binary_hash( m,
//			"110101010101010101010000101010000000000000000001");
//	printf("Hash(110101010101010101010000101010000000000000000001): %u \n",
//			index);
//
//	index = binary_hash( m,
//			"110101010101010101000000000011111111110101010101");
//	printf("Hash(110101010101010101000000000011111111110101010101): %u \n",
//			index);
//
//	index = binary_hash( m,
//			"110101010101010101001010101110000000000000000001");
//	printf("Hash(110101010101010101001010101110000000000000000001): %u \n",
//			index);
//
//	char alphabet_2[] = "0123456789ABCDEFGHIJKLMN�OPQRSTUVWXYZ";
//	m = 400;
//	printf("\nAlphabet: %s\n", alphabet_2);
//	printf("Rango de indices: (0 - %d )\n", m - 1);
//
//	index = hash(alphabet_2, m, "000");
//	printf("Hash(000): %d \n", index);
//
//	index = hash(alphabet_2, m, "FFF");
//	printf("Hash(FFF): %d \n", index);
//
//	index = hash(alphabet_2, m, "ZZZ");
//	printf("Hash(ZZZ): %d \n", index);
//
//	char alphabet_3[] = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";
//	m = 400;
//	printf("\nAlphabet: %s\n", alphabet_3);
//	printf("Rango de indices: (0 - %d )\n", m - 1);
//
//	index = hash(alphabet_3, m, "000");
//	printf("Hash(000): %d \n", index);
//
//	index = hash(alphabet_3, m, "FFF");
//	printf("Hash(FFF): %d \n", index);
//
//	index = hash(alphabet_3, m, "ZZZ");
//	printf("Hash(ZZZ): %d \n", index);
//
//	char alphabet_4[] = "0123456789ABCDEF";
//	m = 100;
//	index = hash2(alphabet_4, "ABCD10", m);
//	printf("Hash(ABCD10): %d \n", index);
//
	return 0;
}

/*prototipo: int cmp_book(Book b1, Book b2);
 */

int cmp_book(Book b1, Book b2) {
	/*Se toma el criterio del Titulo del libro para la comparaci�n.*/
	int b1_len = strlen(b1.title);
	int b2_len = strlen(b2.title);

	int max_cmp = 0;

	if (b1_len <= b2_len) {
		max_cmp = b1_len;
	} else { /*b1_len > b2_len*/
		max_cmp = b2_len;
	}

	Boolean equals = TRUE;

	/*Compara todos los indices suponiendo que */
	int i = 0;
	while (equals && i < max_cmp) {

		/*#include <ctype.h>*/
		if (tolower(b1.title[i]) < tolower(b2.title[i]))
			return 1; /*Char alphabetic value comes first*/
		else if (tolower(b1.title[i]) > tolower(b2.title[i]))
			return -1;
		else
			continue;
		i++;
	}

	return 0; /*equal title*/
}

int compareInt(Type k1, Type k2) {
	int *d1 = (int*) k1;
	int *d2 = (int*) k2;
	return *d1 - *d2;
}

void PrintIntKey(Type key) {
	int *k = (int*) key;
	printf(" %d ", *k);
}

unsigned int hashFunction(Type key, unsigned int size) {
	unsigned int *k = (unsigned int*) key;
	return *k % size;
}

int cmpData(Type d1, Type d2) {
	int *v1 = (int*) d1;
	int *v2 = (int*) d2;
	return *v1 - *v2;
}

Vertex* newVertex(float x, float y, float z) {
	Vertex *v = (Vertex*) malloc(sizeof(Vertex));
	v->x = x;
	v->y = y;
	v->z = z;
	return v;
}

/*
 * Funcion: binary hash
 * Prototipo: unsigned int binary_hash(unsigned int, char*);
 * Descripcion:
 * -	Se utiliza internamente el "abecedario binario" como 01.
 * -	Se implementa la aritmetica de horner para regresar el indice entre 0 - m-1
 */
unsigned int binary_hash(unsigned int m, char *key) {
	char binary_alphabet[] = "01";
	unsigned int index = 0;
	int lk = strlen(key);      // Key Length

	/*Horner*/
	int i = 0;
	index = valueOf(key[i], binary_alphabet) % m;

	while (i < lk - 1) {
		index = ((index * 2) % m) + (valueOf(key[i], binary_alphabet) % m);
		i++;
	}
	return index;
}

unsigned int hash2(char *alphabet, char *key, unsigned int m) {
	unsigned int index = 0;
	int base = strlen(alphabet);   //longitud del alfabeto
	int lk = strlen(key);        //longitud de la llave

	//Metodo de Horner
	int i = 0; //posición del caracter que se encuentra más a la derecha en el key
	index = valueOf(key[i], alphabet) % m;
	//Mientras existan caracteres en key por procesar,
	while (i < lk - 1) {
		index = ((index * base) % m) + (valueOf(key[i], alphabet) % m);
		i++;
	}
	return index;
}

unsigned int valueOf(char character, char *alphabet) {
	int i, value = 0;
	int la = strlen(alphabet);   //longitud del alfabeto
	for (i = 0; i < la; i++)
		if (alphabet[i] == character) //Se asumen que el caracter pertenece al alfabeto
			value = i;
	return value;
}

