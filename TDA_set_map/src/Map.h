
#ifndef MAP_H_
#define MAP_H_

typedef void * Type;

//Apuntador a funciones Hash (llave y valor máximo de registros)
typedef unsigned int (*HashFunction) (Type, unsigned int);

//Función que permite verificar si un dato existe en la tabla o en búsquedas
//Será útil cuando se quiera resolver una coalición
typedef int (*CMP)(Type, Type);

//Tipo de dato para crear nuevas instancias del contenedor Map
typedef struct strMap *Map;

//Función que crea una nueva instancia del contenedor Map
//size       tamaño inicial de la tabla
//density    métrica utilizada para saber cuanto espacio queda libre en la tabla
//gFactor    es el factor utilizado para redimensionar la tabla cuando se requiere más espacio
//Hash       es la función Hash
//CMP        función para comparar
Map map_create(int size, float density, float gFactor, HashFunction h, CMP cmp);

void map_set(Map m, Type key, Type value);

Type map_get(Map m,Type key);

void map_remove(Map m, Type key);

void map_destroy(Map m);

#endif /* MAP_H_ */
