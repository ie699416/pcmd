/*
 * Type.h
 *
 *  Created on: Mar 26, 2020
 *      Author: administrador
 */

#ifndef TYPE_H_
#define TYPE_H_

#define ZERO		0

#define ERR 	   -1
#define FALSE		0
#define TRUE		1

#include <stdio.h>
#include <stdlib.h>

typedef void * Type;

typedef enum {false, true} Boolean;

Type Int(int);

Type Float(float);

Type Char(char);

Type String(char *);

int  Type2Int(Type);

float  Type2Float(Type);

char Type2Char(Type);

char * Type2String(Type);


#endif /* TYPE_H_ */
