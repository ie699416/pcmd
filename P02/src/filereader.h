

#ifndef FILEREADER_H_
#define FILEREADER_H_

#include "mmu.h"
#include <string.h>

/* Function: Read file
 * Prototype: boolean FR_readFile(char *);
 * Input: char*
 * Description:
 * -	The function receives a path on which a Text file is located.
 *
 * -	It creates a local FILE * type variable to read the file.
 * 		>>	Returns FALSE if FILE points to NULL.
 *
 * -	Receives as a second argument the reference of the MMU instance
 * 		so it can store the bytes of the file into the heap through the mmu.
 *
 * -	Calls the MMU_addElement function while !EOF.
 *
 * -	Close the file while exiting the function.
 * 		>> Returns TRUE as the file was read successfully.
 * Returns: boolean.
 */

Boolean FR_readFile(char *, MMU*);



#endif /* FILEREADER_H_ */
