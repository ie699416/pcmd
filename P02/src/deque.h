
#ifndef DEQUE_H_
#define DEQUE_H_

#include "Type.h"



/*********************************************************************************************************************/
/*Public data types definitions*/
/*********************************************************************************************************************/

typedef struct strDeque *  Deque; 		// Private definition at deque.c

/*********************************************************************************************************************/
/*Public functions definitions*/
/*********************************************************************************************************************/

/* Function: Create new deque (DQ) container instance.
 * Doc prototype: createDeque();
 * Prototype: Deque deque_create();
 * Input: void
 * Description:
 * -	The DQ instance is created with dynamic memory using MALLOC. *
 * -	The DQ members are initialized as:
 * 		-	DQ.front: points to NULL.
 * 		-	DQ.rear: points to NULL.
 * 		-	DQ.size: set to ZERO.
 * 		-	DQ.isEmpty: Set to TRUE. *
 * Returns: DQ address.
 */

Deque deque_create();

/* Function: Add new element at the front of the DQ instance.
 * Doc prototype: addFront(Deque, Type);
 * Prototype: void deque_offer_front(Deque, Type);
 * Input: Deque, Type.
 * Description:
 * -	The DQ instance creates a new node (newNode).
 * -	The member DQ.front points to newNode and newNode.previous is linked to the current node at the front.
 * - 	Updates the instance pointer DQ.front to the new node.
 * Returns: DQ address.
 */

void deque_offer_front(Deque, Type);

/* Function: Add new element at the rear of the DQ instance.
 * Doc prototype: addRear(Deque, Type);
 * Prototype: void deque_offer_rear(Deque, Type);
 * Input: Deque, Type.
 * Description:
 * -	The DQ instance creates a new node (newNode).
 * -	The member DQ.rear points to newNode and newNode.next is linked to the current node at the rear.
 * -    Updates the instance pointer DQ.rear to the new node.
 * Returns: DQ address.
 */
void deque_offer_rear(Deque, Type);

/* Function: Get DQ number of elements (size) in the container. (size)
 * Doc prototype: size(Deque);
 * Prototype: unsigned int deque_size(Deque);
 * Input: Deque.
 * Description:
 * -	Returns the value of the member DQ.size.
 * Returns: DQ.size. (Deque->size)
 */

unsigned int deque_size(Deque);

/* Function: Get DQ flag isEmpty at the container.
 * Doc prototype: isEmpty(Deque);
 * Prototype: Boolean deque_isEmpty(Deque);
 * Input: Deque.
 * Description:
 * -	Returns the value of the member DQ.isEmpty. This flag is constantly updated when polling the queue.
 * Returns: DQ.isEmpty. (Deque->isEmpty)
 */

Boolean deque_isEmpty(Deque);

/* Function: Remove element at the top (poll front).
 * Doc prototype: removeFront(Deque);
 * Prototype: Type deque_poll_front(Deque);
 * Input: Deque.
 * Description:
 * -	Polls the element at the front by saving it into a temporal variable (Type tempElement).
 * -	Updates the pointer DQ.front to the previous element.
 * -	The unused node gets freed.
 * - 	Reduce the size of the queue.
 * 		>>	if the size is ZERO,  the member DQ.isEmpty is updated to TRUE.
 * -	Returns the value of the front value stored at the tempElement.
 * Returns: tempElement. (Type tempElement)
 */
Type deque_poll_front(Deque);

/* Function: Remove element at the bottom (poll rear).
 * Doc prototype: removeRear(Deque);
 * Prototype: Type deque_poll_rear(Deque);
 * Input: Deque.
 * Description:
 * -	Polls the element at the rear by saving it into a temporal variable (Type tempElement).
 * -	Updates the pointer DQ.rear to the next element.
 * -	The unused node gets freed.
 * - 	Reduce the size of the queue.
 * 		>>	if the size is ZERO,  the member DQ.isEmpty is updated to TRUE.
 * -	Returns the value of the front value stored at the tempElement.
 * Returns: tempElement. (Type tempElement)
 */
Type deque_poll_rear(Deque);

/* Function: Read element at the top (front peek).
 * Doc prototype: not requested.
 * Prototype: Type queue_peek_front(Deque);
 * Input: Deque.
 * Description:
 * -	Returns the value of the front value pointed by the member DQ.front.
 * Returns: DQ.front. (Deque->front)
 */
Type deque_peek_front(Deque);

/* Function: Read element at the bottom (rear peek).
 * Doc prototype: not requested.
 * Prototype: Type queue_peek_rear(Deque);
 * Input: Deque.
 * Description:
 * -	Returns the value of the rear value pointed by the member DQ.rear.
 * Returns: DQ.rear. (Deque->rear)
 */
Type deque_peek_rear(Deque);

/* Function: Clear container.
 * Prototype: void deque_clear(Deque);
 * Input: Deque.
 * Description:
 * -	Runs an algorithm to empty all nodes, freeing each one of them.
 * Returns: void function.
 */
void deque_clear(Deque);

/* Function: Destroy container.
 * Doc prototype: destroyDeque(Deque);
 * Prototype: void deque_destroy(Deque);
 * Input: Deque.
 * Description:
 * -	Calls void deque_clear(Deque);
 * -	Proceeds to free the reserved space for the container.
 * Returns: void function.
 */
void deque_destroy(Deque);



#endif /* DEQUE_H_ */
