
#ifndef TEXTPROCESSOR_H_
#define TEXTPROCESSOR_H_

#include "mmu.h"
#include <string.h>


typedef struct str_word{
	unsigned int length;	// Size in Bytes.
	unsigned int count;		// Histogram count
	char word [20];			// Self reference to the word with fixed max length 20.
}Word;


typedef struct str_sentence{
	unsigned int words;		// Number of words in the sentences.
	unsigned int size;		// Size in Bytes or chars.
	char * sentence;		// Self reference to the sentence string.
}Sentence;


/* Function: Print Index
 * Prototype: void printBufferIndex(char *, unsigned int);
 * Input: char *, unsigned int
 * Description:  Prints the buffer elements starting at zero until n, where n is expected to be the buffer size.
 * Returns: void
 */

void printBufferIndex(char *, unsigned int );

/* Function: Print Index
 * Prototype: void printBufferText(char *, unsigned int);
 * Input: char *, unsigned int
 * Description:  Prints the buffer string.
 * Returns: void
 */

void printBufferText(char *, unsigned int);


/* Function: Add End Of String character.
 * Prototype: void addEOS(MMU *);
 * Input: MMU *
 * Description: calls MMU_addElement with the reference of a character '\0' to incorporate into the memory.
 * Returns: void
 */

void addEOS(MMU *);

/* Function: Count words
 * Prototype: unsigned int countWords(char * , unsigned int );
 * Input: char * , unsigned int*
 * Description:
 * -	To find a word, we need to make sure the previous value was a letter and the current one is not
 * Returns: unsigned int
 */



unsigned int countWords(char * , unsigned int );

/* Function: Count sentences
 * Prototype: unsigned int countSentences(char * , unsigned int );
 * Input: char * , unsigned int*
 * Description:
 * -	To find a sentences, we need to make sure the previous value was a letter and the current one is a period.
 * Returns: unsigned int
 */

unsigned int countSentences(char * , unsigned int );

/* Function: Check if char correspond to an alphabet symbol.
 * Prototype: boolean isLetter(char * );
 * Input: char *
 * Description:
 * -	This function compares the input and returns true if is the value is different
 * 		from the ASCII values corresponding to the alphabet characters (upper case and lower case)
 * 		>> Upper case: between 65 and 90
 * 		>> Lower case: between 97 and 122
 * Returns: boolean
 */

Boolean isLetter(char * );

/* Function: Check if char correspond to space
 * Prototype: boolean isSpace(char * );
 * Input: char *
 * Description: This function compares the input and returns true if is equal to 32. (' ').
 * Returns: boolean
 */

Boolean isSpace(char * );


/* Function: Check if char correspond to a period
 * Prototype: boolean isSpace(char * );
 * Input: char *
 * Description: This function compares the input and returns true if is equal to 46. ('.').
 * Returns: boolean
 */

Boolean isPeriod(char * );





#endif /* TEXTPROCESSOR_H_ */
