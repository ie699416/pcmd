#include "mmu.h"



void MMU_clean(MMU *instance) {
	for (int i = 0; i < MEMORY_SIZE; i++) {
		instance->addresses[i] = NULL;
	}
	instance->freeMemoryPtr = &(instance->addresses[ZERO]);
	instance->numel = ZERO;
	instance->isEmpty = TRUE;
	instance->isFull = FALSE;
}

MMU* MMU_newInstance(void) {
	MMU *new = (MMU*) malloc(sizeof(MMU));
	new->startPtr = (Type) malloc(MEMORY_SIZE * sizeof(char));
	MMU_clean(new);
	return new;
}

void MMU_destroyInstance(MMU *instance) {
	/* TO BE replaced with MMU function*/
	free(instance->startPtr);
	/**/
	free(instance);
}

void MMU_increaseAndCheck(MMU *instance) {
	instance->numel = instance->numel + 1;
	instance->freeMemoryPtr++;
	if (instance->numel == MEMORY_SIZE) {
		instance->isFull = TRUE;
	}
	instance->isEmpty = FALSE;		//If the buffer was empty, now is not.
}

void MMU_decreaseAndCheck(MMU *instance) {
	instance->numel = instance->numel - 1;
	instance->freeMemoryPtr--;
	if (instance->numel == ZERO) {
		instance->isEmpty = TRUE;
	}
	instance->isFull = FALSE;		//If the buffer was full, now is not.
}

Boolean MMU_addElement(Type element, MMU *instance) {
	unsigned int n;
	if (instance->isFull == FALSE) {
		n = instance->numel;
		instance->addresses[n] = (instance->startPtr) + n;

		char *address = instance->addresses[instance->numel];
		*address = *((char*) (element));
		MMU_increaseAndCheck(instance);
		return TRUE;
	} else {
		return FALSE;
	}
}

Boolean MMU_removeElement(Type element, MMU *instance) {
	return FALSE;
}

Boolean MMU_getIsFull(MMU *instance) {
	return instance->isFull;
}

Boolean MMU_getIsEmpty(MMU *instance) {
	return instance->isEmpty;
}

float MMU_calculateUseRatio(unsigned int bytes) {
	return ((float) bytes / (float) (MEMORY_SIZE));
}

Boolean MMU_checkFit(unsigned int bytes, MMU *instance) {
	if (MMU_calculateUseRatio(bytes)
			< (1 - MMU_calculateUseRatio(instance->numel))) {
		return TRUE;
	} else {
		return FALSE;
	}
}

void MMU_printElements(MMU *instance) {
	printf("\nMemory in use: %.4f %%\n",
			MMU_calculateUseRatio(instance->numel) * 100);
	printf("\nMemory elements: \n");
	char *address;
	for (int i = 0; i < instance->numel; i++) {
		address = MMU_readIndex(instance, i);
		printf("\nLocation : 0x%p ", address);
		printf("| Value : %c", *address);
	}
}

char* MMU_readIndex(MMU *instance, unsigned int index) {

	return ((char*) (instance->addresses[index]));
}

void MMU_readMemory(MMU *instance, unsigned int n, char chars[n], Boolean order) {
	if (order == LIFO) {
		for (int i = 0; i < n; i++) {
			chars[n - i - 1] = *(MMU_readIndex(instance, i));
			;
		}
	} else {
		for (int i = n - 1; i >= ZERO; i--) {
			chars[i] = *(MMU_readIndex(instance, i));
		}

	}

}

int MMU_writeString(MMU *instance, unsigned int n, char *chars) {

	if (MMU_checkFit(n, instance)) {
		printf("\nString Fit\n");
		unsigned int index = instance->numel;
		int i = 0;
		char c;

		while (i < n) {
			c = chars[i];
			;
			MMU_addElement(&c, instance);

			printf("%c", c);
			i++;
		}

		printf("\nBytes stored: %d", n);

		return index;
	} else
		return ERR;
}

