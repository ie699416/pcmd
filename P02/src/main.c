/*
 ============================================================================

 ============================================================================
 */

#include "navigator.h"
#include "filereader.h"
#include "textprocessor.h"
#include "type.h"

int main(void) {
	/*Eclipse buffers flush commands*/
	setvbuf(stdin, NULL, _IONBF, 0);
	setvbuf(stdout, NULL, _IONBF, 0);
	setvbuf(stderr, NULL, _IONBF, 0);

	/*Variables declaration*/
	char folderPath[_MAX_PATH]; 	// Windows Path MAX 260 chars
	char fileName[100];
	int n;								// Number of charactes in file variable
	char *buffer;   					// Pointer to dynamic string.
	Boolean fileExist;					// File read flag
	MMU *MMU_instance;					// MMU instance reference
	int menuOP;							// exit OP code from sub-menu
	unsigned int menu_option;			// Main menu OP code switch.
	Navigator *explorer;					// Navigator instance declaration

	/*Init variables.*/
	MMU_instance = MMU_newInstance();
	menuOP = ZERO;
	n = ZERO;
	buffer = NULL;
	menu_option = ZERO;
	explorer = navigator_createInstance();

	/*TEST*/
//	char *test_string = "C:\\699416\\PcMD\\P02\\Text\\primero.txt";
//	Type type = String(test_string);
//	deque_offer_front((Deque) (explorer->historic_back), type);
	/*End test*/
	printf("\nEnter texts folder path:");
		scanf("%s", folderPath);
		printf("\nFolder path: %s", folderPath);

	setMainPath(folderPath);

	/*User Interface*/
	while (1) {
		printf("\n\n\tMenu options :");
		printf("\n\t\t- [1] Open File.");
		printf("\n\t\t- [2] Exit.");
		printf("\n\t\t- [3] Show recent.");
		printf("\n\t\t- [4] Show previous.");
		printf("\n\t\t- [5] Show next.");
		printf("\n\t\t- [6] Show first.");
		printf("\n\t\t- [7] Show last.");
		printf("\n");

		scanf("%d", &menu_option);

		switch (menu_option) {
		case 1:
			getFilePath(fileName);
			navigator_openFile(explorer, fileName, folderPath);
			break;

		case 2:
			navigator_destroyInstance(explorer);
			MMU_destroyInstance(MMU_instance);
			return ZERO;

		case 3:
			MMU_clean(MMU_instance);
			fileExist = FR_readFile(explorer->current_filePathStr,
					MMU_instance);
			if (fileExist) {
				n = MMU_instance->numel;
				if (NULL != buffer)
					free(buffer);
				buffer = (char*) malloc(n * sizeof(char));
				MMU_readMemory(MMU_instance, n, buffer, FIFO); // Copies memory data into buffer.
				while (1) { 			// Menu Infinite loop
					menuOP = printMenu(buffer, n);
					if (TRUE == menuOP)
						break;
					else if (ZERO == menuOP) {
						continue;
					} else {
						break;
					}
				}

			} else {
				printf("\n Invalid File.");
			}

			break;

		case 4:
			if (navigator_read_back(explorer)) {

				MMU_clean(MMU_instance);
				fileExist = FR_readFile(explorer->current_filePathStr, MMU_instance);
				if (fileExist) {
					n = MMU_instance->numel;
					if (NULL != buffer)
						free(buffer);
					buffer = (char*) malloc(n * sizeof(char));
					MMU_readMemory(MMU_instance, n, buffer, FIFO); // Copies memory data into buffer.
					while (1) { 			// Menu Infinite loop
						menuOP = printMenu(buffer, n);
						if (TRUE == menuOP)
							break;
						else if (ZERO == menuOP) {
							continue;
						} else {
							break;
						}
					}

				} else {
					printf("\n Invalid File.");
				}
			}
			break;

		case 5:
			if (navigator_read_forth(explorer)) {
				MMU_clean(MMU_instance);
				fileExist = FR_readFile(explorer->current_filePathStr, MMU_instance);
				if (fileExist) {
					n = MMU_instance->numel;
					if (NULL != buffer)
						free(buffer);
					buffer = (char*) malloc(n * sizeof(char));
					MMU_readMemory(MMU_instance, n, buffer, FIFO); // Copies memory data into buffer.
					while (1) { 			// Menu Infinite loop
						menuOP = printMenu(buffer, n);
						if (TRUE == menuOP)
							break;
						else if (ZERO == menuOP) {
							continue;
						} else {
							break;
						}
					}

				} else {
					printf("\n Invalid File.");
				}
			}
			break;

		case 6:
			navigator_read_first(explorer, folderPath);
			MMU_clean(MMU_instance);
			fileExist = FR_readFile(explorer->current_filePathStr, MMU_instance);
			if (fileExist) {
				n = MMU_instance->numel;
				if (NULL != buffer)
					free(buffer);
				buffer = (char*) malloc(n * sizeof(char));
				MMU_readMemory(MMU_instance, n, buffer, FIFO); // Copies memory data into buffer.
				while (1) { 			// Menu Infinite loop
					menuOP = printMenu(buffer, n);
					if (TRUE == menuOP)
						break;
					else if (ZERO == menuOP) {
						continue;
					} else {
						break;
					}
				}

			} else {
				printf("\n Invalid File.");
			}

			break;
		case 7:
			navigator_read_last(explorer, folderPath);
			MMU_clean(MMU_instance);
			fileExist = FR_readFile(explorer->current_filePathStr, MMU_instance);
			if (fileExist) {
				n = MMU_instance->numel;
				if (NULL != buffer)
					free(buffer);
				buffer = (char*) malloc(n * sizeof(char));
				MMU_readMemory(MMU_instance, n, buffer, FIFO); // Copies memory data into buffer.
				while (1) { 			// Menu Infinite loop
					menuOP = printMenu(buffer, n);
					if (TRUE == menuOP)
						break;
					else if (ZERO == menuOP) {
						continue;
					} else {
						break;
					}
				}

			} else {
				printf("\n Invalid File.");
			}
			break;
		default:
			continue;
		}
	}

	/*Dynamic Memory release*/
	navigator_destroyInstance(explorer);
	MMU_destroyInstance(MMU_instance);
	return ZERO;
}
