#include "textprocessor.h"

//Upper case: between 65 and 90 ;  Lower case: between 97 and 122
Boolean isLetter(char *value) {
	if ((65 <= *value && 90 >= *value) || (97 <= *value && 122 >= *value)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

Boolean isSpace(char *value) {
	return (' ' == *value) ? TRUE : FALSE;
}

Boolean isPeriod(char *value) {
	return ('.' == *value) ? TRUE : FALSE;
}

unsigned int countWords(char *buffer, unsigned int n) {
	unsigned int words = 0;
	Boolean isLetter_flag = FALSE;
	Boolean wasLetter_flag = FALSE;
	char value;

	for (int i = ZERO; i < n; i++) {
		wasLetter_flag = isLetter_flag; //keeps previous value
		value = buffer[i];
		isLetter_flag = isLetter(&value);

		/*To find a word, we need to make sure the previous value was a letter and the current one is not*/
		if (FALSE == isLetter_flag && TRUE == wasLetter_flag)
			words = words + 1;
	}
	return words;
}

unsigned int countSentences(char *buffer, unsigned int n) {
	unsigned int sentences = 0;
	Boolean isLetter_flag = FALSE;
	Boolean wasLetter_flag = FALSE;
	Boolean isPeriod_flag = FALSE;
	char value;

	for (int i = ZERO; i < n; i++) {
		wasLetter_flag = isLetter_flag; //keeps previous value
		value = buffer[i];
		isPeriod_flag = isPeriod(&value);
		isLetter_flag = isLetter(&value);

		/*To find a word, we need to make sure the previous value was a letter and the current one is not*/
		if (TRUE == wasLetter_flag && TRUE == isPeriod_flag)
			sentences = sentences + 1;
	}
	return sentences;
}

void addEOS(MMU *MMU_instance) {
	char eos = '\0';
	Type *eosPtr = (Type) &eos;
	MMU_addElement(eosPtr, MMU_instance);
}

void printBufferIndex(char *buffer, unsigned int n) {
	printf("\n\nRead elements: \n");
	for (int i = 0; i < n; i++) {
		printf("\nValue : %c", buffer[i]);
		printf(" | Index : %d ", i);
	}
}

void printBufferText(char *buffer, unsigned int n) {
	printf("\n\nRead elements: \n");
	for (int i = 0; i < n; i++) {
		printf("%c", buffer[i]);
	}
}
